/* version 1.1 */
jQuery(document).ready(function($) {
								
	$('.datepicker').
		prop('readonly', true).
		datepicker({
			dateFormat: 'dd-M-yy',
			changeMonth: true,
			changeYear: true
		});

	$('form.save_view_member').submit(function(e){
		$('ul.woocommerce-error').html('');
		var errors = 0;
		$('form.view-member label').each(function(){
			if ( $('span', this).length > 0 ) {
				var name = $(this).attr('for');
				var input = $('input[name="'+name+'"], textarea[name="'+name+'"]');
				if ( input.val().length < 1 ) {
					errors++;
					input.addClass('border-error');
				}
			}
		});
		if ( errors > 0 ) {
			e.preventDefault();
			var message = 'The highlighted field';
			message += ( errors > 1 ) ? 's are' : ' is';
			$('ul.woocommerce-error').removeClass('hidden').append('<li>' + message + ' required</li>');
		} 
	});

	$('input, textarea').click(function(){
		$(this).removeClass('border-error');
	});

});