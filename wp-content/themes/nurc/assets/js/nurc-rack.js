/* version 1.2 */
jQuery(document).ready(function($) {

	$('#member-edit').on( 'click', function(e){
		e.preventDefault();
		$('#select-member').removeClass('hidden');
	});

	$('#member').on( 'click', function(){
		$(this).val('');
	})
	// don't navigate away from the field on tab when selecting an item
	.on( "keydown", function( event ) {
		if ( event.keyCode === $.ui.keyCode.TAB && $( this ).autocomplete( "instance" ).menu.active ) {
		  event.preventDefault();
		}
	})
	.autocomplete({
        minLength: 2,
        source: function( request, response ) {
          $.getJSON( ajaxurl + '?action=nurc_members&security=' + wp_ajax.ajaxnonce + '&term=' + request.term, response );
        },
        select: function( event, ui ) {
        	this.value = '';
	        update_rack(ui.item);
	        return false;
        }
    });

    $('#delete-member').on( 'click', function(e){
    	e.preventDefault();
    	delete_rack_member();
    });

    /* warning if rack number already used */
    $('input[name="rack-no"]').change(function(){
    	$('.rack').html('');
		var data = {
			'action': 'nurc_check_rack',
			'security': wp_ajax.ajaxnonce,
			'rack-no': $(this).val(),
			'post_id': $('#post_ID').val()
		};
	    $.post(ajaxurl, data, function( response ){
	    	if ( response.data ) {
	    		$('.rack').html( 'This number is already taken');
	    	}
	    })
		.always(function(){
			$('.spinner').removeClass('is-active');
		});
	});

});

function update_rack(item) {
	$('.note').html('');
	var data = {
		'action': 'nurc_update_rack',
		'security': wp_ajax.ajaxnonce,
		'id': item.id,
		'post_id': $('#post_ID').val()
	};
    $.post(ajaxurl, data, function( response ){
	    if( !response.success || data < 1 ){
	        $('.note').first().prepend('<span class="red">Could not update</span> ');
	    } else {
			$('#select-member').addClass('hidden');
	        $('#display_name').html(item.label);
	        $('input[name="member"]').val(item.id);
	        $('#member-edit').html('Edit');
			$('.submitbox').removeClass('hidden');
	    }
	})
	.fail(function(){
		$('.note').last().prepend('<span class="red">Could not update</span> ');
	})
	.always(function(){
		$('.spinner').removeClass('is-active');
	});
}


function delete_rack_member() {
	$('.note').html('');
	var data = {
		'action': 'nurc_delete_rack_member',
		'security': wp_ajax.ajaxnonce,
		'id': $('input[name="member"]').val(),
		'post_id': $('#post_ID').val()
	};
    $.post(ajaxurl, data, function( response ){
	    if( !response.success ){
	        $('.note').first().prepend('<span class="red">Could not delete</span> ');
	    } else {
			$('#select-member').addClass('hidden');
			$('.submitbox').addClass('hidden');
	        $('#display_name').html('');
	        $('input[name="member"]').val('');
	        $('#member-edit').html('Add');
	    }
	})
	.fail(function(){
		$('.note').last().prepend('<span class="red">Could not delete</span> ');
	})
	.always(function(){
		$('.spinner').removeClass('is-active');
	});
}