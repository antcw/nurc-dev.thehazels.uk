/* version 1 */
jQuery(document).ready(function($) {

	var variations = $('.variations_form').data('product_variations');
	var compare = 'Choose an option';
	var title = $('h1.product_title').html().split(' ')[0];

	$('table.variations select').change(function(){
		$('#nurc-price').html('');
		$('input[name="payment_details"]').val('');

		if ( $('#pa_payment-method :selected').text() != compare && $('#pa_membership-type :selected').text() != compare ){
			var payidx = $('#pa_payment-method').children('option:selected').index();
			var memidx = $('#pa_membership-type').children('option:selected').index();
			var index = findIndex(memidx, payidx, variations.length);
			var desc = variations[index]['variation_description'];
			$('#nurc-price').append(desc).append(variations[index]['price_html']);
			desc = desc.replace(/<(?:.|\n)*?>/gm, '');
			$('input[name="payment_details"]').val(desc);
		} 
	});

	$('.datepicker').
		prop('readonly', true).
		datepicker({
			dateFormat: 'dd-M-yy',
			changeMonth: true,
			changeYear: true
		});
	
	var defaultDate = null;
	var minDate = null;
	var yearRange = null;
	if ( title == 'Senior' || title == 'Student' ) {
		defaultDate = '-19y';
		yearRange = '-100:+100';
	} else if ( title == 'Junior' ) {
		defaultDate = '-10y';
		yearRange = '-19:+19';
	} 
	$('input[name="date_of_birth"]').datepicker('option', 'defaultDate', defaultDate);
	$('input[name="date_of_birth"]').datepicker('option', 'yearRange', yearRange);
	$('input[name="date_of_birth"]').on('change', function(){
		$('span.red.note').remove();
		var age = getAgeLastSeptember($(this).val());
		if ( title == 'Senior' || title == 'Student' ) {
			if ( age < 18 ) {
				$(this).before('<span class="red note">Age at the end of August this year is less than 18<br>Please choose <a href="/product/junior-membership">Junior membership</a> instead</span>')
			}
		}
		else if ( title == 'Junior' ) {
			if ( age > 17 ) {
				$(this).before('<span class="red note">Age at the end of August this year is over 18<br>Please choose <a href="/product/senior-membership">Senior membership</a> instead</span>')
			}
		}
	});

});

function findIndex( memidx, payidx, varlen ) { 
	var index = 0;

	if ( varlen < 5 ) {
		if      ( memidx == -1 ) index = 0;
		else if ( memidx == 1 && payidx == 1 ) index = 0;
		else if ( memidx == 2 && payidx == 1 ) index = 1;
	} else {
		if      ( memidx == 1 && payidx == 1 ) index = 0;
		else if ( memidx == 1 && payidx == 2 ) index = 1;
		else if ( memidx == 2 && payidx == 1 ) index = 2;
		else if ( memidx == 2 && payidx == 2 ) index = 3;
		else if ( memidx == 3 ) index = 4;
	}

	return index;
}

function getThisAugust() {

	var today = new Date();
	themonth = today.getMonth();
	theyear = today.getFullYear();
	if ( themonth > 7 ) theyear = theyear + 1;
	
	return thedate = new Date( theyear, 7, 31 );
}

function getMonth(monthStr){
    return new Date(monthStr+'-1-01').getMonth();
}

function getAgeLastSeptember(dateString) {
	var sections = dateString.split('-');
    var thisAug = getThisAugust();
    var birthDate = new Date(sections[2], getMonth(sections[1]), sections[0]);
    var age = thisAug.getFullYear() - birthDate.getFullYear();
    var m = thisAug.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && thisAug.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}