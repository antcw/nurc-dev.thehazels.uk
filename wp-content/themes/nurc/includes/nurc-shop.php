<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
#changes to the membership product archive and single product pages

#remove sorting dropdown
function woocommerce_catalog_ordering() {
}


#Show a shop page description on product archives.
function woocommerce_product_archive_description() {
	// Don't display the description on search results page.
	if ( is_search() ) {
		return;
	}

	if ( is_post_type_archive( 'product' ) && 0 === absint( get_query_var( 'paged' ) ) ) {
		$shop_page = get_post( wc_get_page_id( 'shop' ) );
		if ( $shop_page ) {
			$description = wc_format_content( $shop_page->post_content );
			if ( $description ) {
				echo '<h1 class="entry-title">Membership</h1><div class="page-description">' . $description . '</div>'; // WPCS: XSS ok.
			}
		}
	}
}



/**
 * Applied to membership product category only
 *
 */

#remove product image and thumbnail
function woocommerce_show_product_images() {
	
	if ( has_term('membership', 'product_cat') ) {

	} else {
		wc_get_template( 'single-product/product-image.php' );
	}

}
function woocommerce_get_product_thumbnail( $size = 'shop_catalog', $deprecated1 = 0, $deprecated2 = 0 ) {
	
	if ( has_term('membership', 'product_cat') ) {
		return '';
	} else {
		echo woocommerce_get_product_thumbnail(); // WPCS: XSS ok.
	}
	
}


#Hide product meta
function woocommerce_template_single_meta() {

	if ( has_term('membership', 'product_cat') ) {

	} else {
		wc_get_template( 'single-product/meta.php' );
	}
	
}

#Display product description at top of page
add_action( 'woocommerce_single_product_summary', 'nurc_product_description', 6 );
function nurc_product_description() {

	if ( !has_term('membership', 'product_cat') ) return;
	?>
	<div class="entry-content">
		<?php the_content(); ?>
	</div>
	<?php
}

#remove the product data tabs
function woocommerce_output_product_data_tabs() {

	if ( has_term('membership', 'product_cat') ) {

	} else {
  		wc_get_template( 'single-product/tabs/tabs.php' );
  	}
  	
}

#category title not showing on archive-product page
add_action( 'woocommerce_archive_description', 'nurc_show_page_title', 5 );
function nurc_show_page_title() {
	if ( ! apply_filters( 'woocommerce_show_page_title', true ) ) { ?>

		<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>

	<?php }
}

#display selected price at top of form
add_action( 'woocommerce_single_product_summary', 'nurc_show_selected_price', 20 );
function nurc_show_selected_price() {
	?>
	<div id="nurc-price"></div>
	<?php
}


#https://stackoverflow.com/questions/25880460/woocommerce-how-to-edit-the-added-to-cart-message
add_filter ( 'wc_add_to_cart_message_html', 'wc_add_to_cart_message_filter', 10, 2 );
function wc_add_to_cart_message_filter($message, $products) {
	foreach ($products as $key => $value ) {
		$titles[] = get_the_title( $key );
	}
	
	$titles = array_filter( $titles );
	$added_text = sprintf( _n( '%s has been added to your cart.', '%s have been added to your cart.', sizeof( $titles ), 'woocommerce' ), wc_format_list_of_items( $titles ) );

	$message = sprintf( '%s <a href="%s" class="button">%s</a>&nbsp;&nbsp;&nbsp;<a href="%s" class="button">%s</a>',
	                esc_html( $added_text ),
	                esc_url( wc_get_page_permalink( 'cart' ) ),
	                esc_html__( 'View Cart', 'woocommerce' ),
	                esc_url( home_url('/product-category/membership') ),
	                esc_html__( 'Join another member', 'woocommerce' ));

	return $message;
}

#show variation description instead of payment method in basket and checkout
add_filter( 'woocommerce_get_item_data', 'nurc_payment_method', 20, 2 );
function nurc_payment_method( $item_data, $cart_item ) {
    // Get the corresponding WC_Product
    $product_item = $cart_item['data'];

    if(!empty($product_item) && $product_item->is_type( 'variation' ) ) {
        // WC 3+ compatibility
        $description = version_compare( WC_VERSION, '3.0', '<' ) ? $product_item->get_variation_description() : $product_item->get_description();

        foreach ( $item_data as $key => $item ) {
        	if ( $item['key'] == 'Payment method' ) {
        		$item_data[$key]['value'] = $description;
        	}
        }
    }
    
    return $item_data;
}


/**
 * Changes the redirect URL for the Return To Shop button in the cart.
 *
 * @return string
 */
function wc_empty_cart_redirect_url() {
	return home_url('product-category/membership');
}
add_filter( 'woocommerce_return_to_shop_redirect', 'wc_empty_cart_redirect_url' );


add_action( 'admin_bar_menu', 'nurc_remove_shop_link', 999 );
function nurc_remove_shop_link(){

	global $wp_admin_bar;

	$wp_admin_bar->remove_node('view-store');

}


add_filter( 'woocommerce_min_password_strength', 'nurc_min_password_strength', 10, 1 );
/**
 * Change min password strength (0=none, 5=top strength)
 *
 * @author James Kemp (Iconic)
 * @link http://iconicwp.com/decrease-strength-required-woocommerce-passwords/
 * @param int $strength
 * @return int
 */
function nurc_min_password_strength( $strength ) {
    return 2;
}
 

