<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

include('nurc-wccpf.php'); #wc fields factory fields


#process membership order 
/**
 * Add a custom action to order actions select box on edit order page
 *
 * @param array $actions order actions array to display
 * @return array - updated actions
 */
add_action( 'woocommerce_order_actions', 'nurc_add_order_meta_box_action' );
function nurc_add_order_meta_box_action( $actions ) {
    global $theorder;

    #bail if the action has been run
    if ( get_post_meta( $theorder->get_id(), '_wc_order_marked_nurc_membership', true ) ) {
        return $actions;
    }

    #add custom action
    $actions['wc_custom_nurc'] = __( 'Accept membership', 'nurc' );

    return $actions;
}

#meta box on woocommerce order page for nurc_club_status taxonomy
#add_action( 'add_meta_boxes', 'nurc_order_meta' );
function nurc_order_meta() {
    add_meta_box(
        'nurc_order',
        'Club Status',
        'nurc_meta_details',
        'shop_order',
        'side',
        'core'
    );
}
function nurc_meta_details() {

    $terms = get_terms( array(
        'taxonomy'   => 'nurc_member_status',
        'hide_empty' => false,
        'parent'     => 0,
    ) );

    wp_nonce_field( basename( __FILE__ ), 'nurc_order' );

    ?>
    <div id="nurc_error" style="color: red"></div>
    <ul class="categorychecklist">
        <?php foreach ( $terms as $term ) : ?>
        <li>
            <label class="selectit">
                <input type="checkbox" name="nurc_club_status" value="<?php echo $term->term_id; ?>" /> <?php echo $term->name; ?>
            </label>
        </li>
        <? endforeach; ?>  
    </ul>
    <?php
}

/**
 * Creates new NURC Member/s for each item in the order
 *
 * @param \WC_Order $order
 */
add_action( 'woocommerce_order_action_wc_custom_nurc', 'nurc_new_membership_action' );
function nurc_new_membership_action( $order ) {

    global $nurc;

    $order_items = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
    $customer = get_userdata( $order->get_customer_id() );    
    $count = 0;


    foreach ( $order_items as $item_key => $item ) {
        $meta = nurc_wccpf_meta( $item_key );
        if ( $meta ) {
            $count++;
            $membership_type = ucfirst( str_replace('-', ' ', wc_get_order_item_meta( $item_key, 'pa_membership-type', true ) ) );
            preg_match($nurc->regex, $item->get_name() . ' ' . $membership_type, $matches);
            $labels = ( $matches[1] == 'Junior' ) ? $nurc->juniormeta['contact']['fields'] : $nurc->defaultmeta['contact']['fields'];
            $first = ucfirst( nurc_find_meta($meta, $labels['firstname'][0]) );
            $last = ucfirst( nurc_find_meta($meta,  $labels['surname'][0]) );
            $id = wp_insert_post( array(
                'post_type'      => 'nurc',
                'post_author'    => $order->get_customer_id(),
                'post_title'     => $first . ' ' . $last,
                'post_content'   => '',
                'post_status'    => 'publish',
                'comment_status' => 'closed',
                'ping_status'    => 'closed',
            ));
            if ( $id ) {
                wp_set_object_terms( $id, ucfirst($matches[1]), 'nurc_member' );
                wp_set_object_terms( $id, ucfirst($matches[3]), 'nurc_member_type' );
                wp_set_object_terms( $id, 'Active', 'nurc_member_status' );

                $details = $nurc->get_membership_name( $id, 'nurc_member' );

                if ( $details ) {

                    foreach( $details as $thisid => $labels ) {
                        foreach( $labels['fields'] as $key => $label ) {
                            #add_post_meta($post_id, $meta_key, $meta_value, $unique)
                            add_post_meta( $id, $key, nurc_find_meta($meta, $label[0]) );
                        }
                    }
                    #add_post_meta( $id, 'admin-user_id', $order->get_customer_id() );
                    add_post_meta( $id, 'admin-joined',  date(NURC_DATE) );
                    add_post_meta( $id, 'admin-payment', ucfirst( str_replace('-', ' ', wc_get_order_item_meta( $item_key, 'pa_payment-method', true ) ) ) );
                    update_user_meta( $order->get_customer_id(), 'nurc-balance', 0 );

                } else {
                    $count--;
                }
            }
        }
    }

    if ( $count > 0 ) {
        #add the order note
        $message = sprintf( __( 'New membership details added by %s for %s.', 'nurc' ), wp_get_current_user()->display_name, $customer->first_name . ' ' . $customer->last_name );
        $order->update_status('completed', $message);
        
        #add the flag
        update_post_meta( $order->get_id(), '_wc_order_marked_nurc_membership', 'yes' );
    }

}
