<?php
/**
 * Send emails to selected groups of NURC members (included in admin only)
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

class Nurc_Emails {	 

	public static $post_type, $email_sender_options, $taxonomies;

	/**
	 * Construct.
	 */
	public function __construct() {
        global $nurc;
		self::$post_type = 'nurc_email';

		self::$email_sender_options = array(
			'header-image'    => get_option('woocommerce_email_header_image'),
			'base-colour'     => get_option('woocommerce_email_base_color'),
			'body-background' => get_option('woocommerce_email_body_background_color'),
			'body-text'       => get_option('woocommerce_email_text_color'),
		);

        foreach ($nurc->taxonomies as $key => $value) {
            self::$taxonomies[] = $key;
        }

        $this->register_post_types();

        add_filter( 'manage_' . self::$post_type . '_posts_columns', array( $this, 'change_columns' ) );
        add_action( 'manage_' . self::$post_type . '_posts_custom_column', array( $this, 'custom_columns'), 10, 2 );
        add_filter( 'gettext',               array( $this, 'custom_enter_title' ) );
        add_action( 'publish_' . self::$post_type, array( $this, 'send_email' ), 10, 2 );
        add_action( 'in_admin_header',       array( $this, 'check_category_selected' ) );
        add_filter( 'post_updated_messages', array( $this, 'update_messages' ) );

	}


    /**
     * NURC Members custom post type
     * 
     */
    private function register_post_types() {
        
        /* Label object for NURC emails */
        $labels = array (
            'name'                  => 'NURC Emails',
            'singular_name'         => 'NURC Email',
            'add_new'               => 'Add New',
            'add_new_item'          => 'Add New Email',
            'edit_item'             => 'Edit Email',
            'new_item'              => 'New Email',
            'view_item'             => 'View Email',
            'search_items'          => 'Search Emails',
            'not_found'             => 'No emails found',
            'not_found_in_trash'    => 'No emails found in Trash',
            'exclude_from_search'   => true,
        );
        
        /* Custom post type registration for NURC emails */
        register_post_type(
            self::$post_type,
            array (
                'labels'            => $labels,
                'public'            => false,
                'show_ui'           => true,
                'show_in_admin_bar' => true,
                '_builtin'          => false,
                'hierarchical'      => true,
                'rewrite'           => false,
                'query_var'         => self::$post_type,
                'register_meta_box_cb' => array($this, 'add_metaboxes'),
                'taxonomies'        => self::$taxonomies,
                'show_in_menu'      => 'edit.php?post_type=nurc'
            )
        );

    }

    function change_columns( $cols ) {

        $cols = array(
            'cb'       => '<input type="checkbox" />',
            'title'    => __('Email', self::$post_type),
            'sent'     => __('Sent', self::$post_type),
            'groups'   => __('Groups', self::$post_type),
        );
        return $cols;
    }

    function custom_columns( $column, $post_id ) { 

        switch ( $column ) {
            case 'title' :
                $post = get_post($post_id);
                echo $post->post_title;
                break;
            case 'sent' :
                $send_result = get_post_meta( $post_id, 'send-result', true );
                preg_match('/<b>(.*?)<\/b>:<br>Group: (.*?)<br>/', $send_result, $matches);
                if ( count($matches) > 1 ) echo $matches[1];
                break;
            case 'groups' :
                $send_result = get_post_meta( $post_id, 'send-result', true );
                preg_match('/<b>(.*?)<\/b>:<br>Group: (.*?)<br>/', $send_result, $matches);
                if ( count($matches) > 2 ) echo $matches[2];
                break;
            default:
                echo get_post_meta( $post_id, $column, true );
                break;
        }
    }

    #custom post type messages
    function update_messages( $messages ) {

        global $pagenow;
        global $post_type;

        if ( ( $pagenow == 'post-new.php' || $pagenow == 'post.php' ) && $post_type == self::$post_type ) {

            $messages['post'][1] = 'Email updated';
            $messages['post'][4] = 'Email updated';
            $messages['post'][6] = 'Email published';
            $messages['post'][7] = 'Email saved';
            $messages['post'][8] = 'Email submitted';
            $messages['post'][9] = 'Email scheduled for: ';

        }

        return $messages;
    }
    #define metabox for information and details of email once sent
    function add_metaboxes() {

    	add_meta_box(
    		'instructions',
    		'NURC Members Email',
    		array($this, 'meta_instructions'),
    		self::$post_type
    	);
    }
    function meta_instructions( $post, $callback_args ) { 
        
        echo get_post_meta( $post->ID, 'send-result', true );
        ?>
        <hr>
    	<p>
    		Emails are sent to all the members within the group/s selected. Groups are Type, Option and Club Status.
        </p>
        <p>
            For example, tick <b>Senior</b> (Type) and <b>Active</b> (Club Status) to send to all <b>current Senior</b> members.
    	</p>
        <?php
    }


    #sends emails to selected terms; uses general settings email address and from name; colour scheme and logo from Woocommerce email settings
	function send_email( $post_id, $post ) {

        if ( get_post_status($post_id) == 'publish' && $this->check_terms($post_id) > 0 ) {

            $data = $this->get_recipients($post_id);
            $sent = array();
            $notsent = array();
            $result = '<b>' . date(NURC_DATETIME ) . '</b>:<br>';

            if ( count($data['to']) > 0 ) {
                foreach ( $data['to'] as $email ) {
                    $to      = $email;
                    $subject = $post->post_title;
                    $message = $this->set_message($subject, $post->post_content );
                    $headers[] = 'Content-Type: text/html; charset=UTF-8';

                    $mail = wp_mail( $to, $subject, $message, $headers );

                    if ( $mail ) $sent[] = $to; else $notsent[] = $to;
                }

                $result .= 'Group: ' . $data['terms'] . '<br>';
                if ( count($sent) > 0 ) $result .= 'Sent to: ' . implode(',  ', $sent);
                if ( count($notsent) > 0 ) $result .= '<br>Failed to send to: ' . implode(',  ', $notsent);

            } else {
                $result .= 'No email addresses found for ' . $data['terms'];
            }

            $previous = get_post_meta( $post_id, 'send-result', true );
            update_post_meta( $post_id, 'send-result', '<p class="small-note">' . $result . '</p>' . $previous );

        }
    }


    #customise title, publish and update inputs
    function custom_enter_title( $input ) {

        global $post_type;

        if( self::$post_type == $post_type ) {

            switch ($input) {
                case 'Enter title here':
                    return 'Enter email subject here';
                    break;
                case 'Publish':
                    return 'Send email';
                    break;
                case 'Update':
                    return 'Send email';
                    break;
                default:
                    return $input;
                    break;
            }
        }

        return $input;
    }

    #email isn't sent if a term isn't checked
    function check_category_selected_error() {
        ?>
        <div class="notice notice-error is-dismissible">
            <p><?php _e('Warning: Email not yet sent. No group of members (ie Type, Option, Club Status) has been selected.', 'nurc'); ?></p>
        </div>
        <?php
    }
    function check_category_selected() {

        global $pagenow;
        global $post_type;

        if ( ( $pagenow == 'post-new.php' || $pagenow == 'post.php' ) && $post_type == self::$post_type ) {

            global $post_ID;

            if ( get_post_status($post_ID) == 'publish' && $this->check_terms($post_ID) == 0 ) {
                add_action('admin_notices', array($this, 'check_category_selected_error'));
            }
        }
    }

    #counts the number of terms selected
    private function check_terms($post_id){

        $check = 0;
        
        foreach ( self::$taxonomies as $taxonomy ) {
            $check = $check + count( wp_get_post_terms($post_id, $taxonomy ));
        }

        return $check;
    }


    #return array of all email addresses for each selected term in the post
    private function get_recipients($post_id) {
        global $nurc;

        $recipients = array();
        $tax_query  = array();
        $selected   = array();

        #build the tax_query for each taxonomy
        foreach ( self::$taxonomies as $taxonomy ) {
            $terms = wp_get_post_terms($post_id, $taxonomy);


            if ( count($terms) > 0 ) {
                $taxname = get_taxonomy( $taxonomy );

                foreach ($terms as $term) {
                    $selected[] = $term->name . ' (' . $taxname->label . ')';
                }
                $tax_query[] = array(
                    'taxonomy' => $taxonomy,
                    'field'    => 'term_id',
                    'terms'    => $term->term_id
                );
            }
        }

        #run the query
        if ( count($tax_query) > 0 ){
            $args = (array(
                'post_type' => $nurc->posttype,
                'tax_query' => $tax_query,
            ));
            $members = new WP_Query($args);

            if ( $members->found_posts > 0 ) {
                while ( $members->have_posts() ) {
                    $members->the_post();
                    $id = get_the_id();
                    $meta = get_post_meta($id);
                    if ( isset($meta['email'][0]) ) $recipients[]  = $meta['email'][0];
                    if ( isset($meta['email2'][0]) ) $recipients[] = $meta['email2'][0];
                }
            }
        }

        $result['to']    = array_filter(array_unique($recipients), 'strlen'); #remove duplicates and empty values
        $result['terms'] = implode(', ', $selected); #comma separated string of: term (taxonomy)
        
        return $result;
    }

    function set_message($subject, $content){

        ob_start(); ?>

        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;">
            <tbody>
                <tr>
                    <td valign="top" style="padding:0cm 0cm 0cm 0cm;">
                        <div>
                            <p align="center" style="margin-top:0cm;text-align:center;">
                                <img src="<?php echo self::$email_sender_options['header-image']; ?>" title="<?php echo get_bloginfo('name'); ?>" alt="<?php echo get_bloginfo('name'); ?>">
                            </p>
                        </div>
                        <div align="center">
                            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:487.5pt;background:<?php echo self::$email_sender_options['body-background']; ?>;">
                                <tbody>
                                    <tr>
                                        <td valign="top" style="padding:0cm 0cm 0cm 0cm;">
                                            <div align="center">
                                                <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:487.5pt;">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding:27.0pt 36.0pt 27.0pt 36.0pt;background:<?php echo self::$email_sender_options['base-colour']; ?>;border:1px solid <?php echo self::$email_sender_options['base-colour']; ?>">
                                                                <h1 style="margin:0cm;margin-bottom:.0001pt;line-height:150%">
                                                                    <span style="font-size:22.5pt;line-height:150%;font-family:Helvetica,sans-serif;color:<?php echo self::$email_sender_options['body-background']; ?>;font-weight:normal">
                                                                        <?php echo apply_filters( 'the_title', $subject ); ?>
                                                                    </span>
                                                                </h1>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="border:solid 1px #dedede;padding:0cm 0cm 36.0pt 0cm">
                                            <div align="center">
                                                <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:487.0pt;">
                                                    <tbody>
                                                        <tr>
                                                            <td style="background:<?php echo self::$email_sender_options['body-background']; ?>;padding:0cm 0cm 0cm 0cm">
                                                                <table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <td valign="top" style="padding:36.0pt 36.0pt 0cm 36.0pt;font-size:16px;font-family:Helvetica,sans-serif;color:<?php echo self::$email_sender_options['body-text']; ?>">
                                                                                    <?php echo apply_filters( 'the_content', $content ); ?>
                                                                                </td>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="border:none;padding:0cm 0cm 0cm 0cm">
                                            <div align="center">
                                                <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:487.5pt;">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" style="padding:0cm 0cm 0cm 0cm">
                                                                <table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding:36.0pt 36.0pt 36.0pt 36.0pt">
                                                                                <p align="center" style="text-align:center;line-height:125%">
                                                                                    <span style="font-size:9.0pt;line-height:125%;font-family:Helvetica,sans-serif;color:<?php echo self::$email_sender_options['base-colour']; ?>">
                                                                                        <?php echo get_bloginfo('name'); ?>
                                                                                    </span>
                                                                                </p>
                                                                                <p style="font-size:9.0pt;line-height:125%;font-family:Helvetica,sans-serif;color:<?php echo self::$email_sender_options['body-text']; ?>;">
                                                                                    You are receiving this email because you are a registered member of <?php echo get_bloginfo('name'); ?>. You can update all your details in <a style="color:<?php echo self::$email_sender_options['base-colour']; ?>" href="<?php echo home_url('/shop/my-account/membership') ?>">My Account</a>.
                                                                                </p>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>

        <?php $text = ob_get_contents();

        ob_end_clean();

        return $text;

    }

}

$nurc_emails = new Nurc_Emails();