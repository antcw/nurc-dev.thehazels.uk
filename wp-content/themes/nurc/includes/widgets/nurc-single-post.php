<?php
#https://codex.wordpress.org/Widgets_API
// Register and load the widget
function nurc_sp_load_widget() {
    register_widget( 'nurc_sp_widget' );
}
add_action( 'widgets_init', 'nurc_sp_load_widget' );
 
// Creating the widget 
class nurc_sp_widget extends WP_Widget {
 
		function __construct() {
		parent::__construct(
		 
			// Base ID of your widget
			'nurc-sp-widget', 
			 
			// Widget name will appear in UI
			__('NURC Latest Post Widget', 'colormag'), 
			 
			// Widget description
			array( 'description' => __( 'Displays full title and text of most recent post in selected category', 'colormag' ), ) 
		);
	}

	// Creating widget front-end
	 
	public function widget( $args, $instance ) {
		extract( $args );
		extract( $instance );

		global $post;
		$category = isset( $instance[ 'category' ] ) ? $instance[ 'category' ] : '';
		$linkcat  = isset( $instance[ 'link' ] ) ? $instance[ 'link' ] : '';

		$get_featured_posts = new WP_Query( array(
				'posts_per_page' => 1,
				'post_type'      => 'post',
				'category__in'   => $category,
				'post_status'    => 'publish',
		));
		$cat = get_term($category);

		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		while ( $get_featured_posts->have_posts() ):$get_featured_posts->the_post();
		?>
		<div class="nurc-sp-category">
			<header class="page-header">
				<?php echo colormag_colored_category_title($cat->name); ?>
			</header>
			<h4 class="entry-title">
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
			</h4>
			<p>
				<?php the_excerpt(); ?>
				<a class="more-link" title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><span><?php _e( 'Read more', 'colormag' ); ?></span></a
			</p>
		</div>
		<?php
		$links = nurc_get_links($linkcat);

		if ( $links ) :
			$cat = get_term($linkcat);
		?>
		<div class="nurc-sp-links">
			<header class="page-header">
				<h1 class="page-title">
					<span><?php echo $cat->name; ?></span>
				</h1>
			</header>
			<div class="entry-content"><ul>
			<?php
				foreach ( $links as $link ) {
					?>
					<li><a href="<?php echo $link->link_url; ?>" target="<?php echo $link->link_target; ?>"><?php echo $link->link_name; ?></a>
						<span class="nurc_description"><?php echo $link->link_description; ?></span>
					</li>
					<?php
				}
			?>
			</ul></div>
		</div>
		<?php
		endif;
	endwhile;
		echo $args['after_widget'];
	}
	         
	// Widget Backend 
	public function form( $instance ) {
		$nurc_sp['category'] = get_option('default_category');
		$nurc_sp['link']     = get_option('default_link');
		$instance            = wp_parse_args( ( array ) $instance, $nurc_sp );
		$category            = $instance[ 'category' ];
		$link                = $instance[ 'link' ];
		?>
		<p>
			Show full content of latest post from selected category.
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Select category', 'colormag' ); ?>
				:</label>
			<?php wp_dropdown_categories( array( 'show_option_none' => ' ', 'name' => $this->get_field_name( 'category' ), 'selected' => $category ) ); ?>
		</p>
		<p>
			Include links from selected category.
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e( 'Select Link Category', 'colormag' ); ?>
				:</label>
			<?php wp_dropdown_categories( array( 
				'show_option_none' => ' ', 
				'name' => $this->get_field_name( 'link' ), 
				'selected' => $link,
				'taxonomy' => 'link_category'
				) ); ?>
		</p>
		<?php
}
	     
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['category'] = ( ! empty( $new_instance['category'] ) ) ? $new_instance['category'] : '';
		$instance['link'] = ( ! empty( $new_instance['link'] ) ) ? $new_instance['link'] : '';
		return $instance;
	}
} // Class nurc_sp_widget ends here