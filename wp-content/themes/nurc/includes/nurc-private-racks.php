<?php 
/*
 * NURC Private rack custom post type
 */

if (!defined( 'ABSPATH' )) { exit; }

class Nurc_Private_racks {

    var $post_type = 'nurc_rack';

    #meta patterns for private racks
    var $defaultmeta = array(
        'title'    => 'Rack details',
        'context'  => 'normal',
        'priority' => 'default',
        'fields'   => array(
            'rack-no'    => array( 'Rack no', 'number' ),
            'member'    => array( 'Member', 'member' ),
            'known-paid' => array( 'Known paid', 'radio' ),
            'notes'      => array( 'Notes', 'textarea' )
        ),
    );

    public function __construct() {

        $this->register_post_types();
        setlocale(LC_MONETARY, 'en_GB');

        if( is_admin() ) {
            add_filter( 'manage_' . $this->post_type . '_posts_columns', array( $this, 'change_columns' ) );
            add_action( 'manage_' . $this->post_type . '_posts_custom_column', array( $this, 'custom_columns'), 10, 2 );
            add_filter( 'manage_edit-' . $this->post_type . '_sortable_columns', array( $this, 'sortable_columns' ) );
            add_action( 'pre_get_posts', array( $this, 'pre_get_posts' ), 1 );
            add_action( 'add_meta_boxes_' . $this->post_type, array( $this, 'add_metaboxes') );
            add_action( 'save_post',  array( $this, 'save_details' ), 1, 2 );
            add_filter( 'gettext', array( $this, 'custom_enter_title' ) ); 
            include_once('nurc-ajax.php'); #ajax calls
        }
    }
    
    /**
     * NURC Private racks custom post type
     * 
     */
    private function register_post_types() {
        
        $nurc_labels = array (
            'name'                  => 'Private racks',
            'singular_name'         => 'Private rack',
            'add_new'               => 'Add New',
            'add_new_item'          => 'Add New Rack',
            'edit_item'             => 'Edit Rack',
            'new_item'              => 'New Rack',
            'view_item'             => 'View Rack',
            'search_items'          => 'Search Racks',
            'not_found'             => 'No Racks found',
            'not_found_in_trash'    => 'No Racks found in Trash',
            'exclude_from_search'   => true,
        );
        
        register_post_type(
            $this->post_type,
            array (
                'labels'            => $nurc_labels,
                'public'            => false,
                'show_ui'           => true,
                'show_in_admin_bar' => false,
                '_builtin'          => false,
                'hierarchical'      => true,
                'rewrite'           => false,
                'query_var'         => $this->post_type,
                'supports'          => array('title'),
                'show_in_menu'      => 'edit.php?post_type=nurc',
                'capability_type'   => 'nurc',
                'map_meta_cap'      => false,
                'capabilities'      => array(
                    'delete_posts'  => 'delete_nurcs',
                )
            )
        );

    }


    /**
     * Overview page
     *
     */
    function change_columns( $cols ) {
        $cols = array(
            'cb'           => '<input type="checkbox" />',
            'rack-no'      => __('Rack no', $this->post_type),
            'title'        => __('Name / Boat', $this->post_type),
            'member'      => __('Member', $this->post_type),
            'known-paid'   => __('Known paid', $this->post_type),
            'notes'        => __('Notes', $this->post_type)
        );
        return $cols;
    }

    function custom_columns( $column, $post_id ) { 

        switch ( $column ) {
            case 'title' :
                $post = get_post($post_id);
                echo $post->post_title;
                break;
            case 'member' :
                $this->member_url( $post_id );
                break;
            case 'known-paid' :
                echo ucwords( get_post_meta( $post_id, $column, true ) );
                break;
            default:
                echo get_post_meta( $post_id, $column, true );
                break;
        }
    }

    function sortable_columns() {

        return array(
            'rack-no' => 'rack-no',
        );

    }

    function pre_get_posts($query) {
        if( !is_admin() )
            return;

        global $current_screen;

        if( ! is_null($current_screen) && $current_screen->base == 'edit' && $this->post_type == $query->get('post_type') ) {
            $query->set( 'meta_key', 'rack-no' );
            $query->set( 'orderby', 'meta_value_num' );
            $query->set( 'order', 'ASC' );
        }

    }

    function member_url( $post_id ) {

        $member_id = get_post_meta( $post_id, 'member', true );

        if ( $member_id > 0 ) {
            $fullname = get_post_meta( $member_id, 'firstname', true ) . ' ' . get_post_meta( $member_id, 'surname', true );
            echo '<a href="' . admin_url('post.php?post=' . $member_id . '&action=edit') . '">' . $fullname . '</a>';
        }

    }

    /* admin edit page */

    #https://wptheming.com/2010/08/custom-metabox-for-post-type/
    #http://seorave.com/complex-wordpress-meta-boxes/
    #add_meta_box( $id, $title, $callback, $page, $context, $priority, $callback_args );
    function add_metaboxes( $post ) {

        add_meta_box(
            $this->post_type . '-details',
            $this->defaultmeta['title'],
            array( $this, 'meta_details' ),
            $this->post_type,
            $this->defaultmeta['context'],
            $this->defaultmeta['priority'],
            $this->defaultmeta['fields']
        );
    }

    /**
     * Output the HTML for the metaboxes.
     */
    function meta_details( $post, $callback_args ) {

        // Nonce field to validate form request came from current site
        wp_nonce_field( basename( __FILE__ ), $this->post_type );

        // Output the field
        foreach ( $callback_args['args'] as $key => $labels ) {
            ?>
            <label for="<?php echo $key; ?>"><?php echo $labels[0]; ?></label>
            <?php
            switch ( $labels[1] ) {
                case 'textarea': ?>
                    <textarea name="<?php echo $key; ?>"><?php if ( isset($post->$key) ) echo $post->$key; ?></textarea>
                    <?php break;
                case 'radio': ?>
                    <div class="nurc-radio">
                        <input type="radio" name="<?php echo $key; ?>" value="yes" <?php if ( isset($post->$key) ) checked( $post->$key, 'yes' ); ?>" /> Yes&nbsp;&nbsp;
                        <input type="radio" class="widefat" name="<?php echo $key; ?>" value="no" <?php if ( isset($post->$key) ) checked( $post->$key, 'no' ); ?>" /> No&nbsp;&nbsp;
                        <input type="radio" class="widefat" name="<?php echo $key; ?>" value="club" <?php if ( isset($post->$key) ) checked( $post->$key, 'club' ); ?>" /> Club&nbsp;&nbsp;
                        <input type="radio" class="widefat" name="<?php echo $key; ?>" value="coach" <?php if ( isset($post->$key) ) checked( $post->$key, 'coach' ); ?>" /> Coach</div>
                    <?php break;
                case 'number': ?>
                    <input type="number" name="<?php echo $key; ?>" value="<?php if ( isset($post->$key) ) echo $post->$key; ?>" min="1" max="100" required /><span class="rack note"></span>
                    <?php break;
                case 'member': 
                    $member = ( $post->$key > 0 ) ? true : false;
                    if ( $member ) {
                        $fullname = get_post_meta( $post->$key, 'firstname', true ) . ' ' . get_post_meta( $post->$key, 'surname', true );
                        $edit = 'Edit';
                    } else {
                        $fullname = '';
                        $edit = 'Add';
                    } ?>
                    <input type="hidden" name="<?php echo $key; ?>" value="<?php if ( isset($post->$key) ) echo $post->$key; ?>" />
                    <span id="display_name"><?php echo $fullname; ?></span>
                    <button id="member-edit"><?php echo $edit; ?></button>
                     <span class="submitbox<?php if (!$member) echo ' hidden'; ?>"><a href="" class="submitdelete" id="delete-member">Delete</a></span>
                    <span class="note"></span>
                    <div id="select-member" class="hidden">
                        <input type="text" class="third" id="<?php echo $key; ?>" placeholder="Start typing a surname and then select from list" />
                    </div>
                    <?php break;
                default: ?>
                    <input type="text" class="widefat" name="<?php echo $key; ?>" value="<?php if ( isset($post->$key) ) echo $post->$key; ?>" />
                    <?php break;
            }
        }

    }


    /**
     * Save the metabox data
     */
    function save_details( $post_id, $post ) {
    // Return if the user doesn't have edit permissions.
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return $post_id;
        }
        // Verify this came from the our screen and with proper authorization,
        // because save_post can be triggered at other times.
        if ( ! isset( $_POST[$this->post_type] ) || ! wp_verify_nonce( $_POST[$this->post_type], basename(__FILE__) ) ) {
            return $post_id;
        }
        
        $nurc_meta['rack-no']    = ( isset($_POST['rack-no']) )    ? $_POST['rack-no'] : 0;
        $nurc_meta['member']    = ( isset($_POST['member']) )    ? $_POST['member'] : 0;
        $nurc_meta['known-paid'] = ( isset($_POST['known-paid']) ) ? $_POST['known-paid'] : '';
        $nurc_meta['notes']      = ( isset($_POST['notes']) )      ? $_POST['notes'] : '';

        // Cycle through the meta array.
        foreach ( $nurc_meta as $key => $value ) {
            // Don't store custom data twice
            if ( 'revision' === $post->post_type ) {
                return;
            }
            if ( get_post_meta( $post_id, $key, false ) ) {
                // If the custom field already has a value, update it.
                update_post_meta( $post_id, $key, $value );
            } else {
                // If the custom field doesn't have a value, add it.
                add_post_meta( $post_id, $key, $value);
            }
            if ( ! $value ) {
                // Delete the meta key if there's no value
                delete_post_meta( $post_id, $key );
            }
        }
    }

    function custom_enter_title( $input ) {

        global $post_type;

        if( is_admin() && 'Enter title here' == $input && $this->post_type == $post_type )
            return 'Name / Boat';

        return $input;
    }
}

$nurc_rack = new Nurc_Private_racks();
