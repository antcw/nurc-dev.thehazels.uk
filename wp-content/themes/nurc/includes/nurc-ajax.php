<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


#Nurc Member
add_action( 'wp_ajax_nurc_update_author', 'nurc_update_author_callback' );
function nurc_update_author_callback(){

    check_ajax_referer('ajax_post_validation', 'security');

    $id = intval($_POST['id']);
    $post_id = intval($_POST['post_id']);

    if ( $post_id > 0 && $id > 0 ) {

        if ( wp_update_post( array( 'ID' => $post_id, 'post_author' => $id ) ) ) wp_send_json_success();
    }

    wp_send_json_error();
}

#Delete transaction
#array(key, memberid)
#response boolean
add_action( 'wp_ajax_nurc_delete_transaction', 'nurc_delete_transaction_callback' );
function nurc_delete_transaction_callback(){

    check_ajax_referer('ajax_post_validation', 'security');

    $delete = $_POST['key'];
    $memberid = $_POST['memberid'];;

    $deleted = false;

    if ( $memberid > 0 ) {

        $transactions = get_post_meta( $memberid, 'transactions', true );

        #delete from transactions, update balances, update main contact balance
        if ( isset($transactions[$delete]) ){
            
            $contact = nurc_user( $memberid )->ID;
            $balance = get_user_meta( $contact, 'nurc-balance', true );
            update_user_meta( $contact, 'nurc-balance', $balance - $transactions[$delete]['amount'] );

            unset( $transactions[$delete] );
            $transactions = array_values(($transactions));

            $tot = count($transactions);
            $balance = 0;
            for ($i=$tot - 1; $i >= 0; $i--) { 
                $balance += $transactions[$i]['amount'];
                $transactions[$i]['balance'] = $balance;
            }

            update_post_meta( $memberid, 'transactions', $transactions );

            $deleted = true;
        }

        wp_send_json_success(array(
            'transactions' => $transactions,
            'deleted' => $deleted,
        ));        
    } else {
        wp_send_json_error();
    }
}


#Private racks and NURC member

# populates drop down list to add members to race
add_action( 'wp_ajax_nurc_rack_contacts', 'nurc_rack_contacts_callback' );
function nurc_rack_contacts_callback(){

    check_ajax_referer('ajax_post_validation', 'security');

    $term = sanitize_text_field($_GET['term']);

    $args = array(
        'exclude'      => array( 1 ),
        'meta_key'     => 'last_name',
        'meta_value'   => $term,
        'meta_compare' => 'like',
        'orderby'      => 'meta_value',
    );

    $users = get_users ( $args );

    $contacts = [];
    foreach ( $users as $user ) {
        $id = $user->ID;
        $firstname = get_user_meta( $id, 'first_name', true );
        $surname   = get_user_meta( $id, 'last_name', true );

        $contacts[] = (object) [
            'id'    => $id,
            'label' => $firstname . ' ' . $surname,
            'value' => $firstname . ' ' . $surname,
        ];
    }

    wp_send_json( $contacts );

}

#Private racks
add_action( 'wp_ajax_nurc_update_rack', 'nurc_update_rack_callback' );
function nurc_update_rack_callback(){

    check_ajax_referer('ajax_post_validation', 'security');

    $member_id = intval($_POST['id']);
    $rack_id = intval($_POST['post_id']);

    if ( $rack_id > 0 && $member_id > 0 ) {

        global $wpdb;
        $racks = $wpdb->get_results( $wpdb->prepare("SELECT pm.post_id, pm.meta_value FROM {$wpdb->postmeta} pm WHERE pm.meta_key = 'rack'"));
        foreach ($racks as $rack) {
            if ( $rack->meta_value == $rack_id ){
                delete_post_meta( $rack->post_id, 'rack' );
            }
        }

        update_post_meta( $rack_id, 'member', $member_id );
        update_post_meta( $member_id, 'rack', $rack_id );

        wp_send_json_success();
    }

    wp_send_json_error();
}

add_action( 'wp_ajax_nurc_delete_rack_member', 'nurc_delete_rack_member_callback' );
function nurc_delete_rack_member_callback(){

    check_ajax_referer('ajax_post_validation', 'security');

    $member_id = intval($_POST['id']);
    $rack_id = intval($_POST['post_id']);

    if ( $rack_id > 0 && $member_id > 0 ) {

        delete_post_meta( $rack_id, 'member' );
        delete_post_meta( $memberid, 'rack' );

        wp_send_json_success();
    }

    wp_send_json_error();
}


add_action( 'wp_ajax_nurc_check_rack', 'nurc_check_rack_callback' );
function nurc_check_rack_callback(){

    check_ajax_referer('ajax_post_validation', 'security');

    $rack_no = intval($_POST['rack-no']);
    $post_id = intval($_POST['post_id']);

    $found = false;

    if ( $rack_no > 0 && $post_id > 0 ) {

        global $wpdb;

        $results = $wpdb->get_results( "SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = 'rack-no'");

        foreach ($results as $result) {
            if ( ! $found ) {
                $found = $rack_no == $result->meta_value;
            }
        }
    }
    wp_send_json_success($found);
}


# populates drop down list to add members to rack
add_action( 'wp_ajax_nurc_members', 'nurc_members_callback' );
function nurc_members_callback(){

    global $nurc;
    check_ajax_referer('ajax_post_validation', 'security');

    $term = sanitize_text_field($_GET['term']);

    $query = new WP_Query( array( 
            'post_type' => $nurc->post_type,
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'nurc_member_status',
                    'field'    => 'slug',
                    'terms'    => 'active',
                )
            ),
            'meta_query'   => array(
                'relation' => 'and',
                'surname_meta' => array(
                    'key'     => 'surname',
                    'value'   => $term,
                    'compare' => 'like',
                 ),
                'firstname_meta' => array(
                    'key'  => 'firstname',
                    'compare' => 'exists',
                )
           ),
            'orderby'      => array(
                'surname_meta' => 'asc',
                'firstname_meta' => 'asc',
            ),
        ) 
    );

    $entrants = [];
    while ( $query->have_posts() ) {
        $query->the_post();
        $id = get_the_id();
        $firstname = get_post_meta( $id, 'firstname', true );
        $surname = get_post_meta( $id, 'surname', true );

        $entrants[] = (object) [
            'id'    => $id,
            'label' => $firstname . ' ' . $surname,
            'value' => $firstname . ' ' . $surname,
        ];
    }

    wp_send_json( $entrants );

}

#Race entries
# return all members with selected agegroup
add_action( 'wp_ajax_nurc_get_agegroup', 'nurc_get_agegroup');
function nurc_get_agegroup(){

    check_ajax_referer('ajax_post_validation', 'security');

    $agegroup = sanitize_text_field($_POST['agegroup']);

    if ( strlen($agegroup) > 0 ){
        global $wpdb;

        $query = "select $wpdb->postmeta.post_id,
            (select meta_value from $wpdb->postmeta m where m.post_id = $wpdb->postmeta.post_id and m.meta_key = 'surname') as surname,
            (select meta_value from $wpdb->postmeta m where m.post_id = $wpdb->postmeta.post_id and m.meta_key = 'firstname') as firstname
            from $wpdb->postmeta
            inner join $wpdb->posts on $wpdb->postmeta.post_id = $wpdb->posts.ID and post_status = 'publish'
            inner join $wpdb->term_relationships on $wpdb->term_relationships.object_id = $wpdb->postmeta.post_id
            inner join $wpdb->term_taxonomy on $wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id and $wpdb->term_taxonomy.taxonomy = 'nurc_member_status'
            inner join $wpdb->terms on $wpdb->term_taxonomy.term_id = $wpdb->terms.term_id and $wpdb->terms.slug = 'active'
            where meta_key = 'agegroup' and meta_value = '$agegroup'
            order by surname, firstname";

        $results = $wpdb->get_results($query);

        if ( $wpdb->num_rows > 0 ){
            wp_send_json_success($results); 
        } else {
            wp_send_json_error();
        }

    }

}
/* add members to race 'entrants' and fee to member 'transactions'
    post: members: array(id => name, entry, trailer, note)
        string date
        string title
        int raceid
    response: array(agegroup => array(id => fullname, entry, trailer, note))
        int totalentry
        int totaltrailer
        int count
*/ 
add_action( 'wp_ajax_nurc_add_to_race', 'nurc_add_to_race_callback' );
function nurc_add_to_race_callback(){

    check_ajax_referer('ajax_post_validation', 'security');

    $raceid   = intval($_POST['raceid']);

    if ( $raceid > 0 ) {
        $members = json_decode(stripslashes($_POST['members']));

        $date = $_POST['date'];
        if ( strlen($date) < 1 ) $date = date(NURC_DATE);

        $title = sanitize_text_field($_POST['title']);

        $entrants = get_post_meta( $raceid, 'entrants', true );
        if ( !$entrants ) $entrants = array();

        $update = array();
        $noupdate = array();
        $totalentry = get_post_meta( $raceid, 'total-entry', true );
        $totaltrailer = get_post_meta( $raceid, 'total-trailer', true );

        foreach ($members as $member) {
            $memberid = intval($member->memberid);
            $key = 'id-' . $memberid;

            if ( $memberid > 0 ) {

                $agegroup = get_post_meta( $memberid, 'agegroup', true );
                if ( !isset($entrants[$agegroup]) ) $entrants[$agegroup] = array();

                #add if member not already added
                if ( !isset( $entrants[$agegroup][$key] ) ){

                    $entry = floatval($member->entry);
                    $trailer = floatval($member->trailer);
                    $membernote = sanitize_text_field($member->note);

                    $note = $title;
                    if ( $entry > 0 ) $note .= ' entry';
                    if ( $entry > 0 && $trailer > 0 ) $note .= ' and';
                    if ( $trailer > 0 ) $note .= ' trailer';
                    if ( strlen($membernote) > 0 ) $note .= ' (' . $membernote . ')';

                    $amount = $entry + $trailer;
                    $totalentry += $entry;
                    $totaltrailer += $trailer;

                    nurc_update_transactions( $date, $amount, 'fee', $note, $memberid );#
                    $first = get_post_meta( $memberid, 'firstname', true );
                    $last = get_post_meta( $memberid, 'surname', true );

                    $entrants[$agegroup][$key] = array(
                        'first'   => $first,
                        'last'    => $last,
                        'entry'   => $entry,
                        'trailer' => $trailer,
                        'note'    => $membernote,
                    );

                    $update[] = $first . ' ' . $last;

                #otherwise return name for error message
                } else {
                    $noupdate[] = $entrants[$agegroup][$key]['first'] . ' ' . $entrants[$agegroup][$key]['last'];
                }
            }
        }

        update_post_meta( $raceid, 'total-entry', $totalentry );
        update_post_meta( $raceid, 'total-trailer', $totaltrailer );
        
        $sorted = sort_agegroups( $entrants );
        update_post_meta( $raceid, 'entrants', $sorted );

        wp_send_json_success(array(
            'entrants' => $sorted,
            'update'    => $update,
            'totalentry' => floatval($totalentry),
            'totaltrailer' => floatval($totaltrailer),
            'noupdate' => $noupdate
        ));        
    }

    wp_send_json_error();
}

function sort_agegroups($tosort){

    if ( count($tosort) > 0 ) {

        $agegroups = array();

        foreach ($tosort as $agegroup => $entrants) {
            $surname = array();
            $firstname = array();
            foreach ($entrants as $entrant) {
                $surname[] = $entrant['last'];
                $firstname[] = $entrant['first'];
            } 
            array_multisort(
                $surname, SORT_ASC,
                $firstname, SORT_ASC,
                $entrants
            );
            $tosort[$agegroup] = $entrants;
            $agegroups[] = $agegroup;
        }
        if ( count($agegroups) > 0 ){
            array_multisort(
                $agegroups, SORT_ASC, 
                $tosort
            );     
        } 
    }
    return $tosort;
}

#Delete from race entries
#array(key, raceid, title, date)
#response array(entrants, totalentry, totaltrailer, delete
add_action( 'wp_ajax_nurc_delete_from_race', 'nurc_delete_from_race_callback' );
function nurc_delete_from_race_callback(){

    check_ajax_referer('ajax_post_validation', 'security');

    $delete = $_POST['key'];
    $memberid = explode('-', $delete);
    $memberid = intval($memberid[1]);
    $raceid = intval($_POST['raceid']);

    $deleted = false;
    $agegroups = array();
    $totalentry = 0;
    $totaltrailer = 0;

    if ( $memberid > 0 && $raceid > 0 ) {

        $title  = sanitize_text_field($_POST['title']);

        $date   = sanitize_text_field($_POST['date']);
        if ( strlen($date) < 1 ) $date = date(NURC_DATE);

        $totalentry = get_post_meta( $raceid, 'total-entry', true );
        $totaltrailer = get_post_meta( $raceid, 'total-trailer', true );

        $agegroups = get_post_meta( $raceid, 'entrants', true );
        foreach( $agegroups as $agegroup => $entrants ){
            #delete from entrants, update totals, credit member
            if ( isset($agegroups[$agegroup][$delete]) ){
                $entry = $agegroups[$agegroup][$delete]['entry'];
                $trailer = $agegroups[$agegroup][$delete]['trailer'];

                $totalentry -= $entry;
                $totaltrailer -= $trailer;
                update_post_meta( $raceid, 'total-entry', $totalentry );
                update_post_meta( $raceid, 'total-trailer', $totaltrailer );

                $note = $title . ' cancellation credit';
                nurc_update_transactions( $date, $entry + $trailer, 'payment', $note, $memberid );

                $deleted = true;

                unset($agegroups[$agegroup][$delete]);
                update_post_meta( $raceid, 'entrants', $agegroups );
                break;
            }
        }

        wp_send_json_success(array(
            'entrants' => $agegroups,
            'deleted' => $deleted,
            'totalentry' => floatval($totalentry),
            'totaltrailer' => floatval($totaltrailer),
        ));        
    }
}
