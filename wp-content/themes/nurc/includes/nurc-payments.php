<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
#recording of annual and monthly payments (via quick edit members page)
#https://codex.wordpress.org/Plugin_API/Action_Reference/quick_edit_custom_box

add_action( 'quick_edit_custom_box', 'nurc_custom_quickedit', 10, 2 );

function nurc_custom_quickedit( $column_name, $post_type ) {
	global $nurc;

	if ( $post_type != $nurc->post_type ) return;

    static $printNonce = TRUE;
    if ( $printNonce ) {
        $printNonce = FALSE;
        wp_nonce_field( get_stylesheet_directory(), $post_type . '_edit_nonce' );
    }

    #quick edit transaction fields
    if ( $column_name == 'membership' ) { 
    	?>
    	<fieldset class="inline-edit-col-left inline-fieldset-<?php echo $post_type; ?>">
	    	<legend class="inline-edit-legend" id="add-transaction">Add a payment</legend>
	    	<input type="hidden" name="<?php echo $post_type; ?>-title">
	    	<input type="hidden" name="<?php echo $post_type; ?>-contact">
			<div class="inline-edit-col">
		        <label>
		        	<span class="title">Date</span>
		        	<span class="input-text-wrap"><?php echo date(NURC_DATE); ?></span>
		        </label>
		        <label>
		        	<span class="title">Amount<span class="required">*</span></span>
		        	<span class="input-text-wrap">
		        		<input type="number" name="<?php echo $post_type; ?>-amount" />
		        	</span>
		        </label>
		        <label>
		        	<span class="title">Note</span>
		        	<span class="input-text-wrap">
		        		<input type="text" name="<?php echo $post_type; ?>-note" />
		        	</span>
		        </label>
			</div>
	    </fieldset>
    <?php }
}


add_action( 'save_post_nurc', 'nurc_save_custom_quickedit' );
#adds transaction to the transactions meta array
function nurc_save_custom_quickedit( $post_id ) {

    global $nurc;

    if ( !current_user_can( 'edit_post', $post_id ) ) {
        return;
    }

    $_POST += array("{$nurc->post_type}_edit_nonce" => '');
    if ( !wp_verify_nonce( $_POST["{$nurc->post_type}_edit_nonce"], get_stylesheet_directory() ) ) { 
        return;
    }

    if ( !isset( $_REQUEST["{$nurc->post_type}-amount"] ) || $_REQUEST["{$nurc->post_type}-amount"] == 0 ) {
    	echo 'Enter an amount for the transaction';
    	exit;
    }

	$amount  = floatval($_REQUEST["{$nurc->post_type}-amount"]);
	$note    = ( isset($_REQUEST["{$nurc->post_type}-note"]) ) ? $_REQUEST["{$nurc->post_type}-note"] : '';
	$date    = date(NURC_DATE);
	$contact = intval($_REQUEST["{$nurc->post_type}-contact"]);
	$title   = sanitize_text_field($_REQUEST["post_title"]);

	nurc_update_transactions( $date, $amount, 'payment', $note, $post_id, $contact, $title );

}


function nurc_update_transactions( $date, $amount, $type, $note, $post_id, $contact = 0, $title = 0 ){

	global $wpdb;
		
	if ( $type == 'fee' ) $amount = $amount * -1;

	$transactions = get_post_meta( $post_id, 'transactions', true );
	if ( !$transactions ) $transactions = array();

	$transactions[] = array(
		'date'    => $date,
		'amount'  => $amount,
		'note'    => $note,
		'balance' => 0,
	);
	usort( $transactions, 'sort_transactions' );

	#reset balances
	$balance = 0;
	for ($key=count($transactions)-1; $key >=0; $key--) {
		$balance += $transactions[$key]['amount'];
		$transactions[$key]['balance'] = $balance;
	} 

	update_post_meta( $post_id, 'transactions', $transactions );
	#update_post_meta( $post_id, 'balance', $balance );

	#update user's total balance
	if ( $contact == 0 ) $contact = nurc_user( $post_id )->ID;
	$balance = nurc_calc_user_balance( $contact );

	#workaround because quick edit save_post seems to unset post title and main contact
	if ( $contact > 0 && $title > 0 ) {
	    $wpdb->update( $wpdb->posts, 
	    	array( 
	    		'post_title' =>  $title, 
	    		'post_author' => $contact,
	    	), 
	    	array( 'ID' => $post_id ),
	    	array(
	    		'%s',
	    		'%d',
	    	)
	    );		
	}
}
function sort_transactions($a, $b){
	return date_compare($a['date'], $b['date']);
}
#sort in date order, most recent first
function date_compare($a, $b){
	$a = strtotime($a);
	$b = strtotime($b);
    if ($a == $b) {
        return 0;
    }
    return ($a > $b) ? -1 : 1;
}

function reset_transactions($post_id){
	$transactions = get_post_meta( $post_id, 'transactions', true );
	usort( $transactions, 'sort_transactions' );
	#reset balances
	$balance = 0;
	for ($key=count($transactions)-1; $key >=0; $key--) {
	$balance += $transactions[$key]['amount'];
	$transactions[$key]['balance'] = $balance;
	}

	update_post_meta( $post_id, 'transactions', $transactions );
	#update_post_meta( $post_id, 'balance', $balance );

	#update user's total balance
	$contact = get_post_field( 'post_author', $post_id );
	$balance = nurc_calc_user_balance( $contact );


}
function nurc_club_balance(){
    global $wpdb;

    return intval($wpdb->get_var("select sum(meta_value) from $wpdb->usermeta where meta_key = 'nurc-balance'"));

}