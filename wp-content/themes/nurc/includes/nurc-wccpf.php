<?php
#wc fields factory plugin

# extracts relevant meta for order item ($item_id) and decides input format for select order
function nurc_wccpf_meta( $item_id ) {
	global $wpdb;
	
	$query = "select im.meta_key, im.meta_value from wp_woocommerce_order_itemmeta im
				inner join wp_woocommerce_order_items oi on im.order_item_id = oi.order_item_id
				where oi.order_item_id = $item_id and meta_key not like '\_%'
				order by im.meta_id";

	$items = $wpdb->get_results($query);

	if ( nurc_contains($items[0]->meta_value, array('Individual', 'Family')) )
		return $items;
	else
		return false;
}

function nurc_contains($str, array $arr)
{
    foreach($arr as $a) {
        if (stripos($str,$a) !== false) return true;
    }
    return false;
}


function nurc_find_meta( $meta, $tofind ){
	
	foreach ($meta as $key => $value) {
		if ( $value->meta_key == $tofind ) {
			return $value->meta_value;
		}
	}
	return '';

}