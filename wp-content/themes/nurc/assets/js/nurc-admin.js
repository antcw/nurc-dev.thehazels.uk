/* version 1.3 */
jQuery(document).ready(function($) {

	$('.datepicker').
		prop('readonly', true).
		datepicker({
			dateFormat: 'dd-M-yy',
			changeMonth: true,
			changeYear: true
		});

	$('#user-edit').on( 'click', function(e){
		e.preventDefault();
		$('#select-user').removeClass('hidden');
	});

	$('#admin-user_id').on( 'click', function(){
		$(this).val('');
	})
	// don't navigate away from the field on tab when selecting an item
	.on( "keydown", function( event ) {
		if ( event.keyCode === $.ui.keyCode.TAB && $( this ).autocomplete( "instance" ).menu.active ) {
		  event.preventDefault();
		}
	})
	.autocomplete({
        minLength: 2,
        source: function( request, response ) {
          $.getJSON( ajaxurl + '?action=nurc_rack_contacts&security=' + wp_ajax.ajaxnonce + '&term=' + request.term, response );
        },
        select: function( event, ui ) {
        	this.value = '';
	        update_author(ui.item);
	        return false;
        }
    });

	$('.submitdelete').click(function() {
        return window.confirm("Are you sure?");
    });


	//https://codex.wordpress.org/Plugin_API/Action_Reference/quick_edit_custom_box
	// we create a copy of the WP inline edit post function
	if ( typeof inlineEditPost != 'undefined' ){
		var wp_inline_edit = inlineEditPost.edit;

		// and then we overwrite the function with our own code
		inlineEditPost.edit = function( id ) {

			// "call" the original WP edit function
			// we don't want to leave WordPress hanging
			wp_inline_edit.apply( this, arguments );

			// now we take care of our business

			// get the post ID
			var post_id = 0;
			if ( typeof( id ) == 'object' ) {
				post_id = parseInt( this.getId( id ) );
			}

			if ( post_id > 0 ) {
				// define the edit row
				var edit_row = $( '#edit-' + post_id );
				var post_row = $( '#post-' + post_id );

				// get the data
				var title = $( '.post_title', post_row ).text();
				var main_contact = $( '.post_author', post_row ).text();

				// populate the data
				$('#add-transaction').prepend(title + ': ');
				$( ':input[name="nurc-contact"]', edit_row ).val( main_contact );
			}
		};		
	}

    /* delete a transaction */
	$('.delete').click(function(){
        if ( window.confirm("Only delete if there is an error in race fees. It will remove the transaction but does not adjust the race fees") ){
			$('span.red').html('');
			$('span.green').html('');
			delete_transaction($, $(this));
		}
	});

});


function update_author(item) {
	var data = {
		'action': 'nurc_update_author',
		'security': wp_ajax.ajaxnonce,
		'id': item.id,
		'post_id': $('#post_ID').val()
	};
	$('.spinner').addClass('is-active');
    $.post(ajaxurl, data, function( response ){
	    if( !response.success || data < 1 ){
	        $('.note').first().prepend('<span class="red">Could not update</span> ');
	    } else {
			$('#select-user').addClass('hidden');
	        $('#main-contact').html(item.label);
	        $('input[name="admin-user_id"]').val(item.id);
	    }
	})
	.fail(function(){
		$('.note').last().prepend('<span class="red">Could not update</span> ');
	})
	.always(function(){
		$('.spinner').removeClass('is-active');
	});
}



function delete_transaction($, todelete){
	var data = {
		'action'  : 'nurc_delete_transaction',
		'security': wp_ajax.ajaxnonce,
		'key'     : todelete.attr('id'),
		'memberid': $('input[name="post_ID"]').val(),
	};
    $('.spinner').addClass('is-active');
	name = todelete.siblings().eq(2).html();
	$.post(ajaxurl, data, function( response ){
        if( ! response.data.deleted ){
    		$('.note').last().prepend('<span class="red">Could not remove </i>' + name + '</i></span> ');
        } else {
		    var tbody = $('#nurc_payments .nurc_transactions tbody');
			tbody.html('');
	    	$.each(response.data.transactions, function(key, value){
	    		var newTr = $('<tr></tr>');
				newTr.append('<td>' + value.date + '</td>');
				newTr.append('<td class="' + class_colour(value.amount) + '">' + money_format(value.amount) + '</td>');
				newTr.append('<td>' + value.note + '</td>');
				newTr.append('<td class="' + class_colour(value.balance) + '">' + money_format(value.balance) + '</td>');
				var deleteTD = $('<td class="delete red" id="' + key + '">x</td>');
				$(deleteTD).on('click', function(){
					$('span.red').html('');
					$('span.green').html('');
					delete_transaction($, $(this));
				});			
				newTr.append(deleteTD);
				tbody.append(newTr); 
	    	});
			$('.note').last().prepend( '<span class="green"><i>' + name + '</i> deleted</span>' );
        }
    })
    .fail(function(){
    	$('.note').last().prepend('<span class="red"><i>Could not remove ' + name + '</i></span> ');
    })
    .always(function(){
    	$('.spinner').removeClass('is-active');
	});
}

function money_format(num){
	return '£' + num.toFixed(2).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
}

function class_colour(num){
	if (num >= 0) return 'nurc-currency green';
	else return 'nurc-currency red';
}