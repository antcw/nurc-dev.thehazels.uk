<?php
/**
 * My Account Race Entries
 *
 * All associated members race entries and charges
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$count = 0;
?>
	<p>Here you can track payments for race entries and other events</p>

	<table class="woocommerce-order-table woocommerce-MyAccount-order shop_table shop_table_responsive my_account_order account-order-table">
		<thead>
			<tr>
				<?php foreach ( $columns as $column_id => $column_name ) : ?>
					<th class="woocommerce-order-table__header woocommerce-order-table__header-<?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
				<?php endforeach; ?>
			</tr>
			<tr>
				<th style="text-align: right;" colspan="3">Total balance:</th>
				<th style="text-align: right;">
					<span class="<?php echo nurc_class($balance); ?>"><?php echo nurc_money( $balance ); ?></span>
				</th>
				<th></th>
			</tr>
		</thead>
		<tbody>	

		<?php if ( count($members) > 0 ) : 
			foreach( $members as $member ) :
				$id = $member->ID;
		        $transactions = unserialize($member->transactions);
		        if ( $transactions ) : $count++; 
			?>
				<tr class="woocommerce-order-table__row order">
					<td colspan="<?php echo count($columns); ?>" class="woocommerce-orders-table__cell woocommerce-orders-table__cell-fullname" data-title="Name"><?php echo get_post_meta( $id, 'surname', true ) . ', ' . get_post_meta( $id, 'firstname', true ); ?></th>
				</tr>
				<?php foreach( $transactions as $value ) :
					if ( isset($value['amount']) ) : ?>
						<tr>
							<?php foreach ( $columns as $column_id => $column_name ) : ?>
								<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-<?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">
								<?php switch ($column_id) {
									case 'fee':
										echo '<span class="red">';
										if ( $value['amount'] < 0 ) echo nurc_money($value['amount']);
										else echo '-';
										echo '</span>';
										break;
									case 'payment':
										echo '<span class="green">';
										if ( $value['amount'] > 0 ) echo nurc_money($value['amount']);
										else echo '-';
										echo '</span>';
										break;
									case 'balance': 
										echo '<span class="' . nurc_class($value['balance']) . '">' . nurc_money($value['balance']) . '</span>';
										break;
									default:
										if ( strlen($value[$column_id]) > 0 ) echo $value[$column_id];
										else echo '-';
										break;
								} ?>
								</td>
							<?php endforeach; ?>
						</tr>
				        <?php endif; ?>
				    <?php endforeach; ?>
				<?php endif; ?>
			<?php endforeach; 
		endif; ?>

		<?php if ( $count == 0 ) : ?>
			<tr><td colspan="4">No race entries</td></tr>
		<?php endif; ?>

		</tbody>

	</table>

<?php 
/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
