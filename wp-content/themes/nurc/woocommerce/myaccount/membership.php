
<?php
/**
 * My Account Memberships
 *
 * Shows all members for this account.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( count($members) > 0 ) : 
	?>

	<p>
		Current balance with the club (see <a href="<?php echo home_url('/shop/my-account/race-entries'); ?>">Race Fees</a> for full details) : <span class="<?php echo nurc_class($balance); ?>"><?php echo nurc_money( $balance ); ?></span>
	</p>

	<table class="woocommerce-order-table woocommerce-MyAccount-order shop_table shop_table_responsive my_account_order account-order-table">
		<thead>
			<tr>
				<?php foreach ( $columns as $column_id => $column_name ) : ?>
					<th class="woocommerce-order-table__header woocommerce-order-table__header-<?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
				<?php endforeach; ?>
			</tr>
		</thead>
		<tbody>	

		<?php foreach ( $members as $member ) : 
			$id = $member->ID;
		?>
			<tr>
				<?php foreach ( $columns as $column_id => $column_name ) : ?>
					<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-<?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">
					<?php switch ($column_id) {
						case 'name': ?>
							<a href="<?php echo $url; ?>view-member/<?php echo $id; ?>"><?php echo $member->firstname . ' ' . $member->surname;  ?></a>
							<?php break;
						case 'rack':
							$rack_id = get_post_meta( $id, 'rack', true );
							if ( $rack_id > 0 ){
								echo 'Rack no ' . get_post_meta( $rack_id, 'rack-no', true );
							}
							break;
						default:
							echo nurc_taxonomy_list( $id, $column_id );
							break;
					} ?>
					</td>
				<?php endforeach; ?>

		<?php endforeach; ?>

		</tbody>

	</table>

<?php else : ?>
		
	<p>There are no members associated with this account.</p>

<?php endif; ?>

<ul class="products"><li class="product"><a class="button product_type_variable" href="<?php echo home_url('product/private-rack'); ?>">Order a rack</a></li></ul>
<?php
/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
