
<?php
/**
 * My Account View Member
 *
 * Shows all information for selected member
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<ul class="woocommerce-error hidden" role="alert"></ul>
<form class="woocommerce-ViewMemberForm save_view_member" action="" method="post">

<?php
foreach ( $fields as $fieldset ) { ?>
	<fieldset>
		<legend><?php echo $fieldset['title']; ?></legend>
	<?php
	foreach ( $fieldset['fields'] as $key => $labels ) { 
		    $class = ( $labels[1] == 'datepicker' ) ? ' datepicker' : '';
            $required = ( $labels[2] == 'required' ) ? '<span class="required"> *</span>' : '';
            ?>
            <label for="<?php echo $key; ?>"><?php echo $labels[0].$required; ?></label>
            <?php
            switch ( $labels[1] ) {
                case 'textarea': ?>
                    <p><textarea name="<?php echo $key; ?>"><?php if ( isset($meta[$key]) ) echo $meta[$key]; ?></textarea></p>
                    <?php break;
                case 'email': ?>
                    <p><input type="email" class="widefat" name="<?php echo $key; ?>" value="<?php if ( isset($meta[$key]) ) echo $meta[$key]; ?>" /></p>
                    <?php break;
                case 'radio': ?>
                    <div class="nurc-radio"><input type="radio" name="<?php echo $key; ?>" value="yes" <?php if ( isset($meta[$key]) ) checked( $meta[$key], 'yes' ); ?> /> Yes&nbsp;&nbsp;
                    <input type="radio" name="<?php echo $key; ?>" value="no" <?php if ( isset($meta[$key]) ) checked( $meta[$key], 'no' ); ?> /> No</div>
                    <?php break;
                case 'checkbox': ?>
                    <div class="nurc-radio"><input type="checkbox" disabled="disabled" name="<?php echo $key; ?>" value="yes" <?php if ( isset($meta[$key]) ) checked( $meta[$key], 'yes' ); ?> /> Yes</div>
                    <?php break;
                case 'number': ?>
                    <p><input type="number" class="widefat" name="<?php echo $key; ?>" value="<?php if ( isset($meta[$key]) ) echo $meta[$key]; ?>" /></p>
                    <?php break;
                default: ?>
                    <p><input type="text" class="widefat<?php echo $class; ?>" name="<?php echo $key; ?>" value="<?php if ( isset($meta[$key]) ) echo $meta[$key]; ?>" /></p>
                    <?php break;
            }

	} ?>
	</fieldset>
<?php }
?>
    <p>
        <?php wp_nonce_field( 'save_view_member' ); ?>
        <button type="submit" class="woocommerce-Button button" name="save_view_member" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Save changes', 'woocommerce' ); ?></button>
        <input type="hidden" name="action" value="save_view_member" />
        <input type="hidden" name="nurc-id" value="<?php echo $id; ?>" />
    </p>
</form>
<ul class="woocommerce-error hidden" role="alert"></ul>
<?php

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
