/* version 1.4 */
jQuery(document).ready(function($) {

	$('.datepicker').
		prop('readonly', true).
		datepicker({
			dateFormat: 'dd-M-yy',
			changeMonth: true,
			changeYear: true
		});


	/* list members in selected age group */
	$('#nurc-agegroups').change(function(){
		list_agegroup($, $(this).val());
	});

	/* add to race */
	$('#nurc-add').click(function(e){
		e.preventDefault();
		var members = [];
		var count = 0;
		$('input[name="nurc-select[]"]:checked').each(function(){
			var idx = $(this).val();
			members.push({ 
				memberid: idx,
				entry: $('input[name="nurc-entry-' + idx + '"]').val(),
				trailer: $('input[name="nurc-trailer-' + idx + '"]').val(),
				note: $('input[name="nurc-note-' + idx + '"]').val()
			});
			count++;
		});
		if ( count > 0 ) add_to_race( $, members );
	});

    /* delete member from race */
	$('.delete').click(function(){
		$('span.red').html('');
		$('span.green').html('');
		delete_from_race($, $(this));
	});

});

function list_agegroup( $, agegroup ){
    $('.note').last().html('');
    $('#nurc-entrants tbody').html('')
    if ( agegroup == 0 ){
    	$('#nurc-entrants').addClass('hidden');
    } else {
		var data = {
			'action': 'nurc_get_agegroup',
			'security': wp_ajax.ajaxnonce,
			'agegroup': agegroup
		};
		$('.spinner').addClass('is-active');
		$.post(ajaxurl, data, function( response ){
	        if( !response.success ){
	            $('.note').first().prepend('<span class="red">None found</span> ');
	        } else {
				$('#nurc-entrants').removeClass('hidden');
	        	var tbody = $('#nurc-entrants tbody');
				var entry   = $('input[name="entry"]').val();
				var trailer = $('input[name="trailer"]').val();
	        	$.each(response.data, function(key, item){
	        		var tr = $('<tr id="' + item.post_id + '"></tr>');
	        		tr.append('<td style="text-align: center;"><input type="checkbox" name="nurc-select[]" value="' + item.post_id + '" /></td>');
	        		tr.append('<td id="nurc-label-' + item.post_id + '">' + item.firstname + ' ' + item.surname + '</td>');
	        		tr.append('<td style="text-align: center;"><input type="number" max="100" step="0.01" name="nurc-entry-' + item.post_id + '" value="' + entry + '" /></td>');
	        		tr.append('<td style="text-align: center;"><input type="number" max="100" step="0.01" name="nurc-trailer-' + item.post_id + '" value="' + trailer + '" /></td>');
    				tr.append('<td style="text-align: center;"><input type="text" class="widefat" name="nurc-note-' + item.post_id + '" /></td>');
	     	        		tbody.append(tr);
	        	});
	        }
		})
	    .fail(function(){
	    	$('.note').last().prepend('<span class="red">None found</span> ');
	    })
	    .always(function(){
	    	$('.spinner').removeClass('is-active');
		});    	
    }

}

/*  response.data
	entrants array(agegroup => array(id => fullname, entry, trailer, note))
	int entrytotal
	int trailertotal
	noupdate array(name)
*/
function add_to_race( $, members ){
	members = JSON.stringify(members);
	
	$('.note').last().html('');
	var data = {
		'action'  : 'nurc_add_to_race',
		'security': wp_ajax.ajaxnonce,
		'members' : members,
		'raceid'  : $('input[name="post_ID"]').val(),
		'date'    : $('input[name="race-date"').val(),
		'title'   : $('input[name="post_title"').val(),
	};
    $('.spinner').addClass('is-active');
	$.post(ajaxurl, data, function( response ){
        if( !response.success ){
            $('.note').first().prepend('<span class="red">Could not add </span>');
        } else {
        	var note = $('.note').last().html('');
        	race_entrants($, response);
        	if ( response.data.noupdate.length > 0 ){
        		note.append('<span class="red">' + response.data.noupdate.join(', ') + ' already added.</span> ');
        	} 
        	if ( response.data.update.length > 0 ) {
				note.append('<span class="green">' + response.data.update.join(', ') + ' added.</span>');
        	}
        }
    })
    .fail(function(){
    	$('.note').last().prepend('<span class="red">Could not add</span>');
    })
    .always(function(){
    	$('.spinner').removeClass('is-active');
	});
}


function delete_from_race($, todelete){
	var data = {
		'action'  : 'nurc_delete_from_race',
		'security': wp_ajax.ajaxnonce,
		'key'     : todelete.attr('id'),
		'raceid'  : $('input[name="post_ID"]').val(),
		'title'   : $('input[name="post_title"').val(),
		'date'    : $('input[name="race-date"').val(),
	};
    $('.spinner').addClass('is-active');
	name = todelete.siblings('td:first').html();
	$.post(ajaxurl, data, function( response ){
        if( ! response.data.deleted ){
    		$('.note').last().prepend('<span class="red">Could not remove ' + name + '</span> ');
        } else {
        	race_entrants($, response);
            $('.note').last().prepend( '<span class="green">' + name + ' deleted and their account credited. </span>' );
        }
    })
    .fail(function(){
    	$('.note').last().prepend('<span class="red">Could not remove ' + name + '</span> ');
    })
    .always(function(){
    	$('.spinner').removeClass('is-active');
	});
}


function race_entrants($, response){

    var tbody = $('#nurc_race-entrants .nurc_transactions tbody');
	tbody.html('');
	$.each(response.data.entrants, function(agegroup, entrants){
    	tbody.append('<tr><th colspan="5">' + agegroup + '</th></tr>');
    	$.each(entrants, function(id, entrant){
    		var newTr = $('<tr></tr>');
			newTr.append('<td>' + entrant.first + ' ' + entrant.last + '</td>');
			newTr.append('<td class="nurc_currency">' + money_format(entrant.entry) + '</td>');
			newTr.append('<td class="nurc_currency">' + money_format(entrant.trailer) + '</td>');
			newTr.append('<td>' + entrant.note + '</td>');
			var deleteTD = $('<td class="delete red" id="' + id + '">x</td>');
			$(deleteTD).on('click', function(){
				$('span.red').html('');
				$('span.green').html('');
				delete_from_race($, $(this));
			});			
			newTr.append(deleteTD);
			tbody.append(newTr); 
    	});   		
	});
	$('.nurc_transactions th.total-entry').html( money_format(response.data.totalentry) );
	$('.nurc_transactions th.total-trailer').html( money_format(response.data.totaltrailer) );

}

function money_format(num){
	return '£' + num.toFixed(2).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
}