<?php 
/*
 * NURC Race fee custom post type
 */

if (!defined( 'ABSPATH' )) { exit; }

class Nurc_Race_fees {

    var $post_type = 'nurc_race';

    #meta patterns for race entries
    var $defaultmeta = array(
        'title'    => 'Event details',
        'context'  => 'normal',
        'priority' => 'default',
        'fields'   => array(
            'race-date'   => array( 'Race date', 'datepicker' ),
            'entry'       => array( 'Entry', 'number' ),
            'trailer'     => array( 'Trailer', 'number' ),
            'entrants'    => array( 'Add entrants', 'entrant' )
        ),
    );

    public function __construct() {

        $this->register_post_types();
        setlocale(LC_MONETARY, 'en_GB');

        if( is_admin() ) {
            add_filter( 'manage_' . $this->post_type . '_posts_columns', array( $this, 'change_columns' ) );
            add_action( 'manage_' . $this->post_type . '_posts_custom_column', array( $this, 'custom_columns'), 10, 2 );
            add_action( 'posts_clauses', array($this, 'posts_clauses'));
            add_action( 'add_meta_boxes_' . $this->post_type, array( $this, 'add_metaboxes') );
            add_action( 'save_post', array( $this, 'save_details' ), 1, 2 );
            include_once('nurc-ajax.php'); #ajax calls
        }

    }
    
    /**
     * NURC Race Entries custom post type
     * 
     */
    private function register_post_types() {
        
        $nurc_labels = array (
            'name'                  => 'Race fees',
            'singular_name'         => 'Race Fee',
            'add_new'               => 'Add New',
            'add_new_item'          => 'Add New Race Fee',
            'edit_item'             => 'Edit Race Fee',
            'new_item'              => 'New Race Fee',
            'view_item'             => 'View Race Fee',
            'search_items'          => 'Search event',
            'not_found'             => 'No Race fees found',
            'not_found_in_trash'    => 'No Race fees found in Trash',
            'exclude_from_search'   => true,
        );
        
        register_post_type(
            $this->post_type,
            array (
                'labels'            => $nurc_labels,
                'public'            => false,
                'show_ui'           => true,
                'show_in_admin_bar' => false,
                '_builtin'          => false,
                'hierarchical'      => true,
                'rewrite'           => false,
                'query_var'         => $this->post_type,
                'supports'          => array('title'),
                'show_in_menu'      => 'edit.php?post_type=nurc',
                'capability_type'   => 'nurc',
                'map_meta_cap'      => false,
                'capabilities'      => array(
                    'delete_posts'  => 'delete_nurcs',
                )
            )
        );

    }


    /**
     * Overview page
     *
     */
    function change_columns( $cols ) {
        $cols = array(
            'cb'            => '<input type="checkbox" />',
            'title'         => __('Event', $this->post_type),
            'race-date'     => __('Date', $this->post_type),
            'total-entry'   => __('Total entry fees', $this->post_type),
            'total-trailer' => __('Total trailer fees', $this->post_type)
        );
        return $cols;
    }

    function custom_columns( $column, $post_id ) { 

        $value = get_post_meta( $post_id, $column, true );

        switch ( $column ) {
            case 'total-entry' :
            case 'total-trailer' :
                echo nurc_money( $value );
                break;
            case 'race-date' :
                echo ($value) ? date( NURC_DATE, $value ) : $value;
                break;
            default:
                echo $value;
                break;
        }
    }

    function posts_clauses( $pieces ){

        global $pagenow, $post_type;

        if ( 'edit.php' == $pagenow && $this->post_type == $post_type ) {
            
            global $wpdb, $wp_query;

            $pieces['fields'] .= ", (select meta_value from $wpdb->postmeta where $wpdb->posts.ID = post_id and meta_key = 'race-date') as 'racedate'";

            $pieces['orderby'] = 'racedate DESC';

        }
        return $pieces;
    }

    /* admin edit page */

    #https://wptheming.com/2010/08/custom-metabox-for-post-type/
    #http://seorave.com/complex-wordpress-meta-boxes/
    #add_meta_box( $id, $title, $callback, $page, $context, $priority, $callback_args );
    function add_metaboxes( $post ) {

        add_meta_box(
            $this->post_type . '-event',
            $this->defaultmeta['title'],
            array( $this, 'meta_details' ),
            $this->post_type,
            $this->defaultmeta['context'],
            $this->defaultmeta['priority'],
            $this->defaultmeta['fields']
        );

        add_meta_box(
            $this->post_type . '-entrants',
            'Entrants',
            array( $this, 'list_entrants' ),
            $this->post_type
        );
    }

    /**
     * Output the HTML for the metaboxes.
     */
    function meta_details( $post, $callback_args ) {

        global $current_screen, $nurc;

        // Nonce field to validate form request came from current site
        wp_nonce_field( basename( __FILE__ ), $this->post_type ); ?>

        <?php

        // Output the field
        foreach ( $callback_args['args'] as $key => $labels ) {

            if ( $key == 'entrants' && $current_screen->action == 'add' ) {
                echo '<p>You can add entrants once you click on Publish.</p>';
                return;
            }

            if ( $key == 'entrants' ) {
                ?>
                <label for="<?php echo $key; ?>"><?php echo $labels[0]; ?></label>
                <div>
                    <?php $nurc->list_agegroups(); ?>
                    <table id="nurc-entrants" class="nurc_transactions hidden" width="100%">
                        <thead>
                            <tr>
                                <th width="5%">Select</th>
                                <th width="18%">Name</th>
                                <th width="10%">Entry</th>
                                <th width="10%">Trailer</th>
                                <th>Note</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot><tr>
                            <td colspan="5"><input type="submit" value="Add" class="button" id="nurc-add" /></td>
                        </tr></tfoot>
                    </table>
                    <span class="spinner"></span><span class="note"></span>
                </div>
                <?php

            } else {
                $class = ( $labels[1] == 'datepicker' ) ? ' class="datepicker"' : '';
                $name  = ( $labels[1] == 'number' ) ? 'number" step="0.01' : 'text';
                $cur   = ( $labels[1] == 'number' ) ? '£' : '';
                $value =  get_post_meta( $post->ID, $key, true );
                if ( $value && $key == 'race-date' ) $value = date( NURC_DATE, $value );
                ?>
                <label for="<?php echo $key; ?>"><?php echo $labels[0]; ?></label>
                <div>
                    <?php echo $cur; ?><input type="<?php echo $name; ?>"<?php echo $class; ?> name="<?php echo $key; ?>" value="<?php echo $value ?>" />
                </div>
                <?php
            }
        }
    }

    /* entrants: array(agegroup => array(id => first, last, entry, trailer, note)) */
    function list_entrants( $post ) {
        $agegroups = get_post_meta( $post->ID, 'entrants', true );
        $entry = get_post_meta( $post->ID, 'total-entry', true );
        $trailer = get_post_meta( $post->ID, 'total-trailer', true ); ?>

        <span class="spinner"></span>
        <table class="nurc_transactions">
            <thead>
                <tr>
                    <th>Name</th><th>Entry</th><th>Trailer</th><th>Note</th>
                </tr>
            </thead>
            <tbody>
            <?php if ( $agegroups ) : ?>
                <?php foreach ( $agegroups as $agegroup => $entrants ) : ?>
                    <tr><th colspan="5"><?php echo $agegroup; ?></th></tr>
                    <?php foreach ( $entrants as $id => $entrant ) : ?>
                        <tr>
                            <td><?php echo $entrant['first'] . ' ' . $entrant['last']; ?></td>
                            <td class="nurc_currency"><?php echo nurc_money( $entrant['entry'] ); ?></td>
                            <td class="nurc_currency"><?php echo nurc_money( $entrant['trailer'] ); ?></td>
                            <td><?php echo $entrant['note']; ?></td>
                            <td class="delete red" id="<?php echo $id; ?>">x</td>
                        </tr>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th class="nurc_currency">Total:</th>
                    <th class="nurc_currency total-entry"><?php echo nurc_money( get_post_meta( $post->ID, 'total-entry', true ) ); ?></th>
                    <th class="nurc_currency total-trailer"><?php echo nurc_money( get_post_meta( $post->ID, 'total-trailer', true ) ); ?></th>
                    <th colspan="2"></th>
                </tr>
            </tfoot>
        </table>
    <?php }

    /**
     * Save the metabox data
     */
    function save_details( $post_id, $post ) {
    // Return if the user doesn't have edit permissions.
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return $post_id;
        }
        // Verify this came from the our screen and with proper authorization,
        // because save_post can be triggered at other times.
        if ( ! isset( $_POST[$this->post_type] ) || ! wp_verify_nonce( $_POST[$this->post_type], basename(__FILE__) ) ) {
            return $post_id;
        }
        
        $nurc_meta['race-date'] = ( strlen( $_POST['race-date'] ) > 1 ) ? strtotime($_POST['race-date']) : strtotime();
        $nurc_meta['entry']     = ( $_POST['entry'] > 0 ) ? $_POST['entry'] : 0;
        $nurc_meta['trailer']   = ( $_POST['trailer'] > 0 ) ? $_POST['trailer'] : 0;

        // Cycle through the meta array.
        foreach ( $nurc_meta as $key => $value ) {
            // Don't store custom data twice
            if ( 'revision' === $post->post_type ) {
                return;
            }
            if ( get_post_meta( $post_id, $key, false ) ) {
                // If the custom field already has a value, update it.
                update_post_meta( $post_id, $key, $value );
            } else {
                // If the custom field doesn't have a value, add it.
                add_post_meta( $post_id, $key, $value);
            }
            if ( ! $value ) {
                // Delete the meta key if there's no value
                delete_post_meta( $post_id, $key );
            }
        }
        update_post_meta( $post_id, '__nurc-reset-done', true );

        add_action( 'save_post', array( $this, 'save_details' ), 1, 2 );
    }

}

$nurc_race = new Nurc_Race_fees();
