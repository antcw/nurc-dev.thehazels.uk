<?php 
/*
 * NURC Members custom post type
 */

if (!defined( 'ABSPATH' )) { exit; }

class Nurc_Setup {

    var $post_type        = 'nurc';
    var $taxonomies       = array( 
        'nurc_member'=> array(
            'Type',
            array( 'Senior', 'Student', 'Junior', 'Club associate' ),
        ),
        'nurc_member_type' => array(
            'Option',
            array( 'Individual', 'Family' ),
        ),
        'nurc_member_status' => array(
            'Club Status',
            array( 'Active', 'Inactive', 'Lapsed', 'Vacation', 'Coach', 'Committee member', 'President' ),
        )
    );
    #extract membership taxonomies from order item title
    var $regex       = '/(Senior|Student|Junior|Club associate|Gym)(.+)(Individual|Family)/i';

    #meta patterns for memberships
    # metabox id, metabox title, metabox context, metabox priority, fields for order conversion and metabox display (meta id, order meta key, field type, required)
    var $defaultmeta = array(
        'contact'    => array(
            'title'    => 'Contact details',
            'context'  => 'normal',
            'priority' => 'default',
            'fields'   => array(
                'firstname'        => array( 'First name', 'text', 'required' ),
                'surname'          => array( 'Surname', 'text', 'required' ),
                'gender'           => array( 'Gender', 'radio', 'required' ),
                'dob'              => array( 'Date of birth', 'datepicker', 'required' ),
                'address_line_1'   => array( 'Address line 1', 'text', 'required' ),
                'address_line_2'   => array( 'Address line 2', 'text', '' ),
                'postcode'         => array( 'Postcode', 'text', 'required' ),
                'telnumber'        => array( 'Tel home', 'text', '' ),
                'mobile'           => array( 'Mobile', 'text', '' ),
                'email'            => array( 'Email address', 'email', 'required' ),
            ),
        ),
        'emergency'  => array(
            'title'    => 'Emergency contact',
            'context'  => 'side',
            'priority' => 'default',
            'fields'   => array(
                'emergencycontact' => array( 'Emergency contact name', 'text', 'required' ),
                'emergencytel'     => array( 'Tel number',  'text', 'required' ),
                'emergencyrel'     => array( 'Relationship', 'text', '' ),
            ),
        ),
        'swim'       => array(
            'title'    => 'Swim experience',
            'context'  => 'normal',
            'priority' => 'default',
            'fields'   => array(
                'swim'           => array( 'Can you swim 100m in light clothing? ', 'checkbox', 'required' ), 
                'capsize'        => array( 'Have you completed a capsize drill with a British Rowing affiliated rowing club?: ', 'radio', 'required' ),
                'capsizeclub'    => array( 'Capsize drill club', 'text', '' ),
                'capsizedate'    => array( 'Date', 'datepicker', '' ),
            ),
        ),
        'health'     => array(
            'title'    => 'Health and allergies',
            'context'  => 'normal',
            'priority' => 'default',
            'fields'   => array(
                'health'         => array( 'Do you have any known health conditions that may impact your ability to take part in strenuous exercise, and / or to be on water?', 'radio', 'required' ),
                'allergies'      => array( 'Do you have any allergies or take any medications that the club & coaches need to be aware of?', 'radio', 'required' ), 
                'details'        => array( 'If YES, please give details of any health, allergy or medication concerns', 'textarea', '' ),
            ),
        ),
    );
    var $associatemeta = array(
        'contact'      => array(
            'title'    => 'Contact details',
            'context'  => 'normal',
            'priority' => 'default',
            'fields'   => array(
                'firstname'     => array( 'First name', 'text', 'required' ),
                'surname'       => array( 'Surname', 'text', 'required' ),
                'telnumber'     => array( 'Tel number', 'text', '' ),
                'email'         => array( 'Email address', 'email', 'required' ),
            ),
        ),
    );
    var $juniormeta    = array(
        'contact'      => array(
            'title'    => 'Contact details',
            'context'  => 'normal',
            'priority' => 'default',
            'fields'   => array(
                'firstname'      => array( "Junior's name", 'text', 'required' ),
                'surname'        => array( "Junior's surname", 'text', 'required' ),
                'gender'         => array( 'Gender', 'radio', 'required' ),
                'dob'            => array( 'Date of birth', 'datepicker', 'required' ),
                'address_line_1' => array( 'Address line 1', 'text', 'required' ),
                'address_line_2' => array( 'Address line 2', 'text', '' ),
                'postcode'       => array( 'Postcode', 'text', 'required' ),
            ),
        ),
        'emergency'    => array(
            'title'    => 'Emergency contact',
            'context'  => 'side',
            'priority' => 'high',
            'fields'   => array(
                'parent'         => array( 'Parents name(s)', 'textarea', 'required' ),
                'telnumber'      => array( 'Tel home', 'text', '' ),
                'mobile'         => array( 'Parents mobile(s)', 'text', 'required' ),
                'email'          => array( 'Parents email', 'email', 'required' ),
                'email2'         => array( 'Parents second email', 'email', '' ),
                'otheremergency' => array( 'Emergency name', 'textarea', '' ),
                'othertel'       => array( 'Emergency tel', 'text', '' ),
            ),
        ),
        'promotion'    => array(
            'title'    => 'Video and photo consent',
            'context'  => 'normal',
            'priority' => 'default',
            'fields'   => array(
                'promotion'      => array( 'Promotion video / photo consent', 'radio', 'required' ),
                'coaching'       => array( 'Please accept that video will be regularly used as a coaching tool', 'checkbox', 'required' ),
            ),
        ),
        'swim'       => array(
            'title'    => 'Swim experience',
            'context'  => 'normal',
            'priority' => 'default',
            'fields'   => array(
                'swim'           => array( 'Can the junior member swim 100m in light clothing?', 'checkbox', 'required' ), 
                'capsize'        => array( 'Has the junior member completed a capsize drill with any British Rowing affiliated rowing club?', 'radio', 'required' ),
                'capsizeclub'    => array( 'Capsize drill club', 'text', '' ),
                'capsizedate'    => array( 'Capsize drill date', 'datepicker', '' ),
            ),
        ),
        'health'     => array(
            'title'    => 'Health and allergies',
            'context'  => 'normal',
            'priority' => 'default',
            'fields'   => array(
                'health'         => array( 'Does the junior member have any known health conditions that may impact their ability to take part in strenuous exercise, and / or to be on water? ', 'radio', 'required' ),
                'allergies'      => array( 'Does the junior member have any allergies or take any medications that the club & coaches need to be aware of?', 'radio', 'required' ), 
                'details'        => array( 'If YES to either question, please give details', 'textarea', '' ),
            ),
        ),
    );

    var $adminmeta  = array(
        'title'    => 'Admin',
        'context'  => 'side',
        'priority' => 'high',
        'fields'   => array(
            'admin-user_id'  =>  array( 'Main contact', 'user', 'required' ),
            'admin-joined'   =>  array( 'Joined', 'datepicker', 'required' ),
            'admin-payment'  =>  array( 'Payment method', 'text', 'required' ),
        ),
    );

    var $transactionmeta  = array(
        'title'    => 'Transactions',
        'context'  => 'normal',
        'priority' => 'default',
        'fields'   => array(
            'transactions' => array( 'Transactions', 'transactions', '' ),
        ),
    );

    #values - array for radio options, text for one tick checkbox
    var $radio_options = array(
        'gender'    => array('male', 'female'),
        'capsize'   => array('yes', 'no'),
        'health'    => array('yes', 'no'),
        'allergies' => array('yes', 'no'),
        'promotion' => array('yes', 'no'),
        'swim'      => 'yes',
        'coaching'  => 'yes',
    );

    #columns (used for columns, sort and search criteria)
    var $meta_cols = array(
        'fullname'   => 'Name',
        'agegroup'   => 'Age group',
        'membership' => 'Membership',
        'capsize'    => 'Capsize drill',
        'health'     => 'Health',
        'allergies'  => 'Allergies',
        'gender'     => 'Gender',
        'rack'       => 'Private rack'
    );

    public function __construct() {

        $this->register_post_types();
        $this->register_taxonomies();
        setlocale(LC_MONETARY, 'en_GB');

        if( is_admin() ) {
            add_action( 'admin_menu',  array( $this,'register_admin_menus' ) );
            add_filter( 'manage_' . $this->post_type . '_posts_columns',  array( $this, 'change_columns' ) );
            add_action( 'manage_' . $this->post_type . '_posts_custom_column', array( $this, 'custom_columns'), 10, 2 );
            add_filter( 'list_table_primary_column', array( $this, 'list_table_primary_column' ), 10, 2 );
            add_filter( 'post_row_actions', array( $this, 'post_row_actions'), 10, 2 );
            add_filter( 'bulk_actions-edit-' . $this->post_type, array( $this, 'bulk_actions' ) );
            add_filter( 'handle_bulk_actions-edit-' . $this->post_type, array( $this, 'handle_bulk_actions' ), 10, 3 );
            add_filter( 'manage_edit-' . $this->post_type . '_sortable_columns', array( $this, 'sortable_columns' ) );
            add_filter( 'get_user_option_edit_' . $this->post_type . '_per_page', array( $this, 'screen_options_pagination'), 10, 3 );
            add_action( 'restrict_manage_posts', array( $this, 'taxonomy_filters' ), 10, 2 );
            add_filter( 'parse_query', array( $this, 'taxonomy_filter_post_type_request' ) );
            add_filter( 'posts_clauses', array( $this, 'posts_clauses') );
            add_filter( 'get_user_option_meta-box-order_' . $this->post_type, array( $this, 'submitdiv_at_top' ) );
            add_action( 'save_post', array( $this, 'save_member_details' ), 1, 2 );
            include_once('nurc-ajax.php'); #ajax calls
        }
    }
    
    /**
     * NURC Members custom post type
     * 
     */
    private function register_post_types() {
        
        /* Label object for NURC members */
        $nurc_labels = array (
            'name'                  => 'NURC Members',
            'singular_name'         => 'NURC Members',
            'add_new'               => 'Add New',
            'add_new_item'          => 'Add New NURC Member',
            'edit_item'             => 'Edit NURC Member',
            'new_item'              => 'New NURC Member',
            'view_item'             => 'View NURC Member',
            'search_items'          => 'Search by name',
            'not_found'             => 'No NURC Members found',
            'not_found_in_trash'    => 'No NURC Members found in Trash',
            'exclude_from_search'   => true,
        );
        
        /* Custom post type registration for NURC Members */
        register_post_type(
            $this->post_type,
            array (
                'labels'            => $nurc_labels,
                'public'            => false,
                'show_ui'           => true,
                'menu_icon'         => 'dashicons-shield-alt',
                'show_in_admin_bar' => false,
                #'hierarchical'      => true,
                'rewrite'           => false,
                'supports'          => array('title'),
                'register_meta_box_cb' => array($this, 'add_metaboxes'),
                'taxonomies'        => array('nurc_member', 'nurc_member_type'),
                'capability_type'   => 'nurc',
                'map_meta_cap'      => false,
                'capabilities'      => array(
                    'delete_posts'  => 'delete_nurcs',
                )
            )
        );

    }

    #https://yoast.com/dev-blog/custom-post-type-snippets/
    private function register_taxonomies() {

        foreach ( $this-> taxonomies as $key => $label ) {

            /* Label object for private membership category */
            $nurc_labels = array (
                'label'              => __($label[0], $this->post_type),
                'labels'             => array(
                    'name'              => __($label[0], $this->post_type),
                    'singular_name'     => __($label[0], $this->post_type),
                    'menu_name'         => __($label[0] . 's', $this->post_type),
                    'all_items'         => __($label[0] . 's', $this->post_type),
                    'edit_item'         => __('Edit ' . $label[0], $this->post_type),
                    'view_item'         => __('View ' . $label[0], $this->post_type),
                    'update_item'       => __('Update ' . $label[0], $this->post_type),
                    'add_new_item'      => __('Add New ' . $label[0], $this->post_type),
                    'new_item_name'     => __('New ' . $label[0] . ' Name', $this->post_type),
                    'parent_item'       => __('Main ' . $label[0] . ' Type', $this->post_type),
                    'parent_item_colon' => __('Main ' . $label[0] . ' Type:', $this->post_type),
                    'search_items'      => __('Search ' . $label[0] . ' Types', $this->post_type),
                    'not_found'         => __('None found', 'nurc'),
                ),
                'public'             => false,
                'rewrite'            => false,
                'show_ui'            => true,
                'show_in_quick_edit' => false,
                'description'        => __($label[0] . ' definitions', $this->post_type),
                'hierarchical'       => true,
            );

            /* Registration */
            register_taxonomy(
                $key,
                $this->post_type,
                $nurc_labels
            );

            /* Category terms */
            $newterms = $label[1];
            foreach ( $newterms as $term ) {
                wp_insert_term(
                    $term,
                    $key
                );
            }
        }
    }
    

    /**
     * Admin menu
     * remove categories from the menu
     */
    public function register_admin_menus() {
        global $submenu;
       
        unset($submenu['edit.php?post_type=' . $this->post_type][10]);#Add new
        unset($submenu['edit.php?post_type=' . $this->post_type][15]);#Types
        unset($submenu['edit.php?post_type=' . $this->post_type][16]);#Option
        unset($submenu['edit.php?post_type=' . $this->post_type][17]);#Club Status
        
    }

    /**
     * Overview page
     *
     */
    function change_columns( $cols ) {

        $default = array(
            'cb' => '<input type="checkbox" />'
        );
        $cols = array_merge( $default, $this->meta_cols );

        return $cols;
    }

    #add quick edit links to fullname
    function list_table_primary_column( $default, $screen ) {
        if ( 'edit-nurc' === $screen ) {
            $default = 'fullname';
        }
        return $default;
    }

    function custom_columns( $column, $post_id ) { 

        $terms = wp_get_post_terms( $post_id, 'nurc_member' );
        $membership = '';
        foreach ($terms as $term) {
            $membership .= $term->name . ', ';
        }
        $terms = wp_get_post_terms( $post_id, 'nurc_member_type' );
        $type = '';
        foreach ($terms as $term) {
            $type .= $term->name . ', ';
        }
        $terms = wp_get_post_terms( $post_id, 'nurc_member_status' );
        $status = '';
        foreach ($terms as $term) {
            $status .= $term->name . ', ';
        }

        switch ( $column ) {
            case 'fullname':
                $name = $this->get_fullname($post_id); ?>
                <strong><a class="row-title" href="<?php echo admin_url('post.php?post=' . $post_id . '&action=edit'); ?>" aria-label="<?php echo $name; ?> (Edit)"><?php echo $name; ?></a></strong>
                <div class="hidden" id="inline_<?php echo $post_id; ?>">
                    <div class="post_title"><?php echo $name; ?></div>
                    <div class="post_author"><?php echo get_post_field('post_author', $post_id); ?></div>
                </div>
                <?php break;
            case 'membership':
                echo rtrim($membership, ', ') . '/' . trim($type, ', ') . '<br>' . rtrim($status, ', ');
                break;
            case 'agegroup':
                echo $this->get_agegroup($post_id);
                break;
            case 'rack':
                $rack_id = get_post_meta( $post_id, $column, true );
                if ( $rack_id > 0 ) {
                    $rack_no = get_post_meta( $rack_id, 'rack-no', true );
                    echo '<a href="' . admin_url('post.php?post=' . $rack_id . '&action=edit') . '">Rack no: ' . $rack_no . '</a>';
                }
                break;
            default:
                echo ucfirst(get_post_meta( $post_id, $column, true ));
        }
    }

    function sortable_columns() {

        $cols = array();

        foreach ( $this->meta_cols as $col => $title ) {
            $cols[$col] = $col;
        }
        unset( $cols['membership'] );

        return $cols;

    }

    #rename quick edit
    function post_row_actions( $actions, WP_Post $post ) {
        if ( $post->post_type != $this->post_type ) {
            return $actions;
        }

        $quickedit = str_replace('Quick&nbsp;Edit', 'Add&nbsp;a&nbsp;payment', $actions['inline hide-if-no-js']);
        $actions['inline hide-if-no-js'] = $quickedit;
        return $actions;
    }
 
    function bulk_actions($bulk_actions) {
      $bulk_actions['female'] = __( 'Female', 'female');
      $bulk_actions['male'] = __( 'Male', 'male');
      return $bulk_actions;
    }

    function handle_bulk_actions( $redirect_to, $doaction, $post_ids ){

        if ( $doaction == 'male' || $doaction == 'female' ) {

            foreach ( $post_ids as $post_id ) {
                update_post_meta( $post_id, 'gender', $doaction );
            }
            $redirect_to = add_query_arg( 'gender_change', count($post_ids), $redirect_to );

        }
        return $redirect_to;
    }

    function screen_options_pagination($result) {
            return 200;
    }

 
     #filter menus
    function taxonomy_filters($post_type, $which){

        if( $this->post_type !== $post_type ){
            return; //check to make sure this is your cpt
        }

        foreach( $this->taxonomies as $key => $label ) {

            $selected = '';
            if ( isset($_REQUEST[$key] ) ) {
                $selected = $_REQUEST[$key]; //in case the current page is already filtered
            }
            wp_dropdown_categories(array(
                'show_option_all' =>  ($label[0] == 'Club Status') ? $label[0] : $label[0] . 's',
                'taxonomy'        =>  $key,
                'name'            =>  $key,
                'orderby'         =>  'name',
                'selected'        =>  $selected,
                'show_count'      =>  true, // Show number of post in parent term
                'hide_empty'      =>  false, // Don't show posts w/o terms
            ));
        }

        $sel = ( isset($_REQUEST['agegroup']) ) ? $_REQUEST['agegroup'] : '';
        $this->list_agegroups($sel);
    }


    #filter query
    function taxonomy_filter_post_type_request( $query ) {
        global $pagenow;
        $post_type = get_query_var( 'post_type' );

        if ( 'edit.php' == $pagenow && $this->post_type == $post_type ) { 

            $filters = get_object_taxonomies( $this->post_type );

            foreach ( $filters as $tax_slug ) {
                $var = &$query->query_vars[$tax_slug];
                
                if ( isset( $var ) && $var > 0 ) {
                    $term = get_term_by( 'id', $var, $tax_slug );
                    $var = $term->slug;
                }
            }
            if ( isset($_REQUEST['agegroup']) && $_REQUEST['agegroup'] !== '0' ){
                $query->query_vars['meta_key'] = 'agegroup';
                $query->query_vars['meta_value'] = $_REQUEST['agegroup'];
                $query->query_vars['meta_compare'] = '=';
                $query->query_vars['nurc_member_status'] = 'active';
            }
        }
    }


    #set up sorting and search to use meta values
    #array(where, groupby, join, orderby, distinct, fields, limits)
    function posts_clauses( $pieces ){

        global $pagenow, $post_type;

        if ( 'edit.php' == $pagenow && $this->post_type == $post_type ) {
            
            global $wpdb, $wp_query;

            #search
            if ( is_search() && $wp_query->get('s') != '' ){
                $pieces['where'] .= " AND pm.meta_value LIKE '%{$wp_query->get('s')}%'";
                $pieces['where'] .= " AND pm.meta_key IN ('surname', 'firstname')";
                $pieces['join'] .= " INNER JOIN $wpdb->postmeta pm ON pm.post_id = $wpdb->posts.ID";
            }

            #ordering - set defaults if not defined
            $orderby = ( isset( $wp_query->query_vars['orderby'] ) ) ? $wp_query->query_vars['orderby'] : 'fullname';
            $order = ( isset( $wp_query->query_vars['order'] ) ) ? $wp_query->query_vars['order'] : 'ASC';
            $pieces['orderby'] = '';

            if ( $orderby != 'fullname' ){
                $pieces['orderby']  = $orderby . ' ' . $order . ', ';
            }
            $pieces['orderby'] .= 'surname ASC, firstname ASC';
            
            $pieces['distinct'] = 'DISTINCT';

            #set up meta fields for sorting
            foreach( $this->meta_cols as $meta => $title ){
                if ( $meta == 'fullname' ){
                    $pieces['fields'] .= ", (select meta_value from $wpdb->postmeta where $wpdb->posts.ID = post_id and meta_key = 'surname') as 'surname'";
                    $pieces['fields'] .= ", (select meta_value from $wpdb->postmeta where $wpdb->posts.ID = post_id and meta_key = 'firstname') as 'firstname'";
                } else {
                    $pieces['fields'] .= ", (select meta_value from $wpdb->postmeta where $wpdb->posts.ID = post_id and meta_key = '$meta') as '$meta'";
                }
            }
        }
        return $pieces;
    }


    /* admin edit page */

    #https://wptheming.com/2010/08/custom-metabox-for-post-type/
    #http://seorave.com/complex-wordpress-meta-boxes/
    #add_meta_box( $id, $title, $callback, $page, $context, $priority, $callback_args );
    function add_metaboxes( $post ) {

        $meta = $this->get_membership_name( $post->ID, 'nurc_member' );

        if ( $meta ) {

            add_meta_box(
                'nurc_admin',
                $this->adminmeta['title'],
                array( $this, 'meta_details' ),
                $this->post_type,
                $this->adminmeta['context'],
                $this->adminmeta['priority'],
                array( $this->adminmeta['fields'] )
            );

            foreach ( $meta as $id => $values ) {

                add_meta_box(
                    'nurc_' . $id,
                    $values['title'],
                    array( $this, 'meta_details' ),
                    $this->post_type,
                    $values['context'],
                    $values['priority'],
                    array( $values['fields'] )
                );

            }

            add_meta_box(
                'nurc_payments',
                $this->transactionmeta['title'],
                array( $this, 'meta_details' ),
                $this->post_type,
                $this->transactionmeta['context'],
                $this->transactionmeta['priority'],
                array( $this->transactionmeta['fields'] )
            );
        }

    }

    function submitdiv_at_top(){
        return array(
            'side' => join(
                ',',
                array(
                    'submitdiv',
                    'nurc_admin',
                    'nurc_emergency',
                )
            ),
        );
    }

    /**
     * Output the HTML for the metaboxes.
     */
    function meta_details( $post, $callback_args ) {

        // Nonce field to validate form request came from current site
        wp_nonce_field( basename( __FILE__ ), 'nurc_member_details' );

        // get post meta
        $meta = $this->get_nurc_meta( $post->ID, $callback_args['args'][0] );
        // Output the field
        foreach ( $callback_args['args'][0] as $key => $labels ) {
            $class = ( $labels[1] == 'datepicker' ) ? ' datepicker' : '';
            ?>
            <label for="nurc_<?php echo $key; ?>"><?php echo $labels[0]; ?></label>
            <?php
            switch ( $labels[1] ) {
                case 'textarea': ?>
                    <textarea name="<?php echo $key; ?>"><?php echo $meta[$key]; ?></textarea>
                    <?php break;
                case 'email': ?>
                    <input type="email" class="widefat" name="<?php echo $key; ?>" value="<?php echo $meta[$key]; ?>" />
                    <?php break;
                case 'radio': ?>
                    <div class="nurc-radio">
                        <?php foreach ( $this->radio_options[$key] as $option ) : ?>
                            <input type="radio" name="<?php echo $key; ?>" value="<?php echo $option; ?>" <?php checked( $meta[$key], $option ); ?>" /> <?php echo ucfirst($option); ?>&nbsp;&nbsp;
                        <?php endforeach; ?>
                    </div>
                    <?php break;
                case 'checkbox': $option = $this->radio_options[$key] ?>
                    <div class="nurc-radio">
                        <input type="checkbox" name="<?php echo $key; ?>" value="<?php echo $option; ?>" <?php checked( $meta[$key], $option ); ?>" /> <?php echo ucfirst($option); ?>
                        </div>
                    <?php break;
                case 'user':
                    $user = nurc_user( $post->ID ); ?>
                    <div class="nurc-user" id="display_name">
                        <span id="main-contact"><?php if ($user) echo $user->first_name . ' ' . $user->last_name; ?></span> <button id="user-edit">Edit</button>
                        <div class="note"></div>
                    </div>
                    <div id="select-user" class="hidden">
                        <input type="hidden" name="<?php echo $key; ?>" value="<?php if ( $user ) echo $user->ID; ?>" />
                        <input type="text" class="widefat" id="<?php echo $key; ?>" placeholder="Start typing a surname, select from list" />
                    </div>
                    <?php break;
                case 'number': ?>
                    <input type="number" class="widefat" name="<?php echo $key; ?>" value="<?php echo $meta[$key]; ?>" />
                    <?php break;
                case 'transactions': 
                    $this->display_transactions( unserialize($meta[$key]), $post->ID );
                    break;
                default: ?>
                    <input type="text" class="widefat<?php echo $class; ?>" name="<?php echo $key; ?>" value="<?php echo $meta[$key]; ?>" />
                    <?php break;
            }
        }
    }

    function get_nurc_meta( $id, $fields ) {
        $postmeta = get_post_meta( $id );

        foreach ( $fields as $key => $labels ) {
            $meta[$key] = '';
        }

        if ( $postmeta ) {
            foreach ( $postmeta as $key => $item )  {
                $meta[$key] = $item[0];
            }
        } 

        return $meta;
    }

    /**
     * Save the metabox data
     */
    function save_member_details( $post_id, $post ) { 
    // Return if the user doesn't have edit permissions.
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return $post_id;
        }
        // Verify this came from the our screen and with proper authorization,
        // because save_post can be triggered at other times.
        if ( ! isset( $_POST['nurc_member_details'] ) || ! wp_verify_nonce( $_POST['nurc_member_details'], basename(__FILE__) ) ) {
            return $post_id;
        }
        // Now that we're authenticated, time to save the data.
        $meta = $this->get_membership_name( $post_id, 'nurc_member' );

        foreach ( $meta as $labels ) {
            foreach ( $labels['fields'] as $key => $label ) {
                if ( $label[1] == 'textarea' ) {
                    $nurc_meta[$key] = sanitize_textarea_field($_POST[$key]);
                } else {
                    $nurc_meta[$key] = sanitize_text_field($_POST[$key]);
                }
                
            }
        }

        #admin meta box
        $labels = $this->adminmeta['fields'];
        foreach ( $labels as $key => $label ) {
            if ( $label[1] !== 'user' ) {
                $nurc_meta[$key] = sanitize_text_field($_POST[$key]);
            }
        } 


        // Cycle through the meta array.
        foreach ( $nurc_meta as $key => $value ) {
            // Don't store custom data twice
            if ( 'revision' === $post->post_type ) {
                return;
            }
            if ( get_post_meta( $post_id, $key, false ) ) {
                // If the custom field already has a value, update it.
                update_post_meta( $post_id, $key, $value );
            } else {
                // If the custom field doesn't have a value, add it.
                add_post_meta( $post_id, $key, $value);
            }
            if ( ! $value ) {
                // Delete the meta key if there's no value
                delete_post_meta( $post_id, $key );
            }
        }

        #override author with main contact details
        // unhook this function so it doesn't loop infinitely
        remove_action( 'save_post', array( $this, 'save_member_details' ), 1, 2 );

        // update the post, which calls save_post again
        wp_update_post( array( 'ID' => $post_id, 'post_author' => $_POST['admin-user_id'] ) );

        // re-hook this function
        add_action( 'save_post', array( $this, 'save_member_details' ), 1, 2 );
    }


    function get_membership_name( $id, $taxonomy ) {

        $membership = wp_get_post_terms( $id, $taxonomy );

        if ( ! is_wp_error($membership ) && count($membership) > 0 ) { 

            switch ( $membership[0]->name ) {
                case 'Junior':
                    return $this->juniormeta;
                    break;
                
                case 'Club associate':
                    return $this->associatemeta;
                    break;

                default:
                    return $this->defaultmeta;
                    break;
            }
        } else {

            return $this->defaultmeta;
        }
    }


    function user_url( $post_id ) {

        $user = nurc_user( $post_id );

        if ( $user ) : ?>
            <a href="<?php echo admin_url('user-edit.php?user_id=' . $user->ID ); ?>"> <?php echo $user->first_name . ' ' . $user->last_name; ?></a>
        <?php endif;

    }

    /* array
     * date, amount, note, type, balance
     */
    function display_transactions( $transactions, $post_id ) { 
        if ( $transactions ) :
        ?>
            <div><span class="spinner"></span><span class="note"></span></div>
            <table class="nurc_transactions">
                <thead>
                    <tr>
                        <th width="15%">Date</th><th width="10%">Amount</th><th width="60%">Note</th><th width="10%">Balance</th><th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach( $transactions as $key => $value ) :
                    ?>
                    <tr>
                        <td><?php if ( isset( $value['date'] ) ) echo $value['date']; ?></td>
                        <td class="nurc-currency <?php echo nurc_class($value['amount']); ?>"><?php echo nurc_money( $value['amount'] ); ?></td>
                        <td><?php if ( isset( $value['note'] ) ) echo $value['note']; ?></td>
                        <td class="nurc-currency <?php echo nurc_class($value['balance']); ?>"><?php echo nurc_money( $value['balance'] ); ?></td>
                        <td class="delete red" id="<?php echo $key; ?>">x</td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php
        else :
            echo '<div>None</div>';
        endif;
    }


    #calculate agegroup for active members
    private function get_agegroup($post_id){

        $agegroup = '';

        if ( has_term( 'active', 'nurc_member_status', $post_id ) ) {

            $dob = get_post_meta( $post_id, 'dob', true );
            $month = date('n');
            $year = date('Y');
            if ( $month > 8 ) $year++;
            $diff = date_diff(date_create($dob), date_create('31-08-' . $year ));
            $age = $diff->format('%y');

            if ( $age > 18 ) {
                $agegroup = 'Senior';
            } elseif ( $age > 10 ) {
                $agegroup = 'J' . $age;
                $gender = get_post_meta( $post_id, 'gender', true );

                if ( $gender == 'female' ) $agegroup = 'W' . $agegroup;
            }
        }

        update_post_meta( $post_id, 'agegroup', $agegroup );

        return $agegroup;

    }

    function list_agegroups($selected = ''){

        global $wpdb;

        $query = "select meta_value, count(*) as tot from $wpdb->postmeta
            inner join $wpdb->posts on $wpdb->postmeta.post_id = $wpdb->posts.ID and post_status = 'publish'
            inner join $wpdb->term_relationships on $wpdb->term_relationships.object_id = $wpdb->postmeta.post_id
            inner join $wpdb->term_taxonomy on $wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id and $wpdb->term_taxonomy.taxonomy = 'nurc_member_status'
            inner join $wpdb->terms on $wpdb->term_taxonomy.term_id = $wpdb->terms.term_id and $wpdb->terms.slug = 'active'
            where meta_key = 'agegroup' and meta_value <> '' 
            group by meta_value order by meta_value";

        $results = $wpdb->get_results($query);
        ?>

        <select id="nurc-agegroups" name="agegroup">
            <option value="0">Age Groups (active)</option>
        <?php
            foreach ($results as $value) {
                $current = $value->meta_value;
                ?>
                <option value="<?php echo $current; ?>"<?php selected($selected, $current); ?>><?php echo $current . ' (' . $value->tot . ')'; ?></option>
                <?php
            }
        ?>
        </select>

        <?php
    }

    function get_fullname( $post_id ){

        $fullname = get_post_meta( $post_id, 'firstname', true ) . ' ' . get_post_meta( $post_id, 'surname', true );

        return ucwords($fullname);
    }

}

$nurc = new Nurc_Setup();
