/* version 1 */
jQuery(document).ready(function($) {

	$('#post').submit(function(e){
		e.preventDefault();
		if ( $('select[name="wc_order_action"] option:selected').text() == 'Accept membership' &&  ! $('input[name="nurc_club_status"]:checked').length > 0 ) {
			$('#nurc_error').html('<p>Please tick at least one club status</p>');
		} else {
			$('#post').submit();
		}
	});

	$('input[name="nurc_club_status"]').click(function(){
		$('#nurc_error').html();
	});

});