<?php
/**
 * Cron task : payment reminder
 *
 * settings page 
 *   enables cron and frequency
 *   emails sent according to chase amount set
 *   subject and text defined
 *   optional email summary sent to an admin
 * 
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

include('nurc-emails.php'); #use member taxonomies to send emails

class Nurc_Cron {

	public static $options_group, $post_type, $default_options, $chase_options;

	/**
	 * Construct.
	 */

	public function __construct() {

        self::$options_group =  'nurc_options';

        self::$post_type = Nurc_Emails::$post_type;

        self::$default_options = array(
            'enabled' => 0,
            'frequency' => 15,
            'amount' => 5,
            'chase_subject' => 'Payment reminder',
            'chase_text' => 
'Please could you check your payments to the club. Our records show you owe %%total%%.

Log in to %%myaccount%% to see full details in the Race Fees section',
            'email_admin' => '',
        );

        self::$chase_options = wp_parse_args( get_option( self::$options_group ), self::$default_options );

        add_action( 'admin_menu', array( $this, 'admin_menu' ) );
        add_action( 'admin_init', array( $this, 'register_settings' ) );
        add_action( 'nurc_cron_hook', array( $this, 'chase_email') );

    }

    function admin_menu(){
        add_submenu_page( 'edit.php?post_type=nurc', 'Payment reminder', 'Payment reminder', 'manage_options', self::$post_type, array( $this, 'reminder_options_page') );
    }

    function register_settings(){
        register_setting( self::$options_group, self::$options_group, array( $this,  'options_validate' ) );
        add_settings_section( self::$options_group . '_main', 'Settings', array( $this, 'main_text' ), self::$post_type );
        add_settings_field('enable', 'Enabled', array( $this, 'enabled_text' ), self::$post_type, self::$options_group . '_main' );
        add_settings_field('frequency', 'Day of month', array( $this, 'frequency_text' ), self::$post_type, self::$options_group . '_main' );
        add_settings_field('amount', 'Chase amount', array( $this, 'amount_text' ), self::$post_type, self::$options_group . '_main'  );
        add_settings_field('chase_subject', 'Email subject', array( $this, 'subject_text' ), self::$post_type, self::$options_group . '_main'  );
        add_settings_field('chase_text', 'Email text', array( $this, 'chase_text' ), self::$post_type, self::$options_group . '_main'  );
        add_settings_field('email_admin', 'Email admin summary', array( $this, 'email_text' ), self::$post_type, self::$options_group . '_main'  );
    }

    function reminder_options_page(){
        ?>
        <div class="wrap">
            <h2>Payment reminder</h2>
            <?php settings_errors(); ?>
            <form method="post" action="options.php"> 
                <?php 
                settings_fields( self::$options_group );
                do_settings_sections( self::$post_type );
                submit_button();
                submit_button( 'Reset Defaults', 'secondary', 'reset', false );
                ?>
            </form>
            <?php 
            if ( self::$chase_options['enabled'] == 1 ) : ?>
                <p><b>Next email</b>: <?php echo $this->get_next_email_date( self::$chase_options['frequency'] ); ?></p>
            <?php endif; ?>
        </div>
        <?php
    }

    #array(enabled, frequency, amount, chase_subject, chase_text)
    #reset cron schedule and return sanitised values
    function options_validate( $input ){

        $timestamp = wp_next_scheduled( 'nurc_cron_hook' );
        wp_unschedule_event( $timestamp, 'nurc_cron_hook' );

        if ( isset( $_POST['reset'] ) ){

            $input = self::$default_options;
            $message = __('Settings reset to default values');

        } else {

            $enabled = ( isset($input['enabled']) ) ? 1 : 0;
            $frequency = $input['frequency'];
            $message = false;

            if ( $enabled == 1 ){
                wp_schedule_event( time(), 'daily', 'nurc_cron_hook' );
                $date = 
                $message = sprintf( __('Settings updated. Next email will be sent %s'), $this->get_next_email_date( intval($frequency) ) );
            } else {
                $message = __('Settings updated. No emails will be sent');
            }

            $input['frequency'] = intval($input['frequency']);
            $input['chase_subject'] = sanitize_text_field($input['chase_subject']);
            $input['chase_text'] = esc_textarea($input['chase_text']);

        }

        if ( $message )
            add_settings_error(
                self::$options_group . '-validate',
                esc_attr( 'settings_updated' ),
                $message,
                'updated'
            );
        
        return $input;
    }

    function enabled_text(){
        
        $current = self::$chase_options['enabled'];        ?>
        <input type="checkbox" name="<?php echo self::$options_group; ?>[enabled]" value="1" <?php checked( $current, 1 ); ?>/>
        <?php
    }

    function main_text(){

        echo apply_filters( 'the_content', __("A reminder email is sent out monthly on the day specified (<i>day of month</i>) to all members who owe the club the <i>chase amount</i> or more.") ); 
    }

    function frequency_text(){

        $current = self::$chase_options['frequency'];
        echo '<input type="number" min="1" max="28" name="' . self::$options_group . '[frequency]" value="' . $current . '" />';
    }

    function amount_text(){

        $current = self::$chase_options['amount'];
        echo '£ <input type="number" min="0" max="50" step="0.01" name="' . self::$options_group . '[amount]" value="' . $current . '" /> owed';
    }

    function subject_text() {
 
        $current = self::$chase_options['chase_subject'];
        echo '<input type="text" class="regular-text" name="' . self::$options_group . '[chase_subject]" value="' . $current . '" />';
    }

    function chase_text(){

        $current = self::$chase_options['chase_text'];
        ?>
        <textarea class="large-text" rows="10" cols="50" name="<?php echo self::$options_group; ?>[chase_text]" value="<?php echo $current; ?>"><?php echo $current; ?></textarea>
        <p class="nonessential"><?php echo __("%%total%% will be replaced by the amount that they owe the club<br>%%myaccount%% is converted to a link to "); ?><a href="<?php echo home_url('shop/myaccount') ?>" target="_blank">My Account</a></p>
        <?php
    }

    function email_text() {
 
        $current = self::$chase_options['email_admin'];
        echo '<input type="text" class="regular-text" name="' . self::$options_group . '[email_admin]" value="' . $current . '" /> Include an email address to receive a summary of email addresses and monies owed';
    }


    #https://developer.wordpress.org/plugins/cron/scheduling-wp-cron-events/

    #cron run daily; email sent when date matches day specified (chase_options['frequency'])
    function chase_email(){

        $options = self::$chase_options;

        if ( $options['frequency'] !== intval(date('j')) ) return;

        global $nurc_emails, $wpdb;

        $chase_text = $this->filter_myaccount( $options['chase_text'] );

        $content = $nurc_emails->set_message($options['chase_subject'], $chase_text);
        $amount = $options['amount'] * -1;

        $query = "select user_email, (meta_value * -1) as owed from $wpdb->users
                inner join $wpdb->usermeta on $wpdb->users.ID = $wpdb->usermeta.user_id
                where meta_key = 'nurc-balance' and meta_value <= $amount and meta_value <> 0";
        $emails = $wpdb->get_results($query);

        $headers[] = 'Content-Type: text/html; charset=UTF-8';
        $sent = array();
        $notsent = array();
        $email_admin = ( strlen($options['email_admin']) > 0 );

        foreach ($emails as $email) {
            $to      = $email->user_email;
            $subject = $options['chase_subject'];
            $message = $this->filter_total( $content, $email->owed );

            $mail = wp_mail( $to, $subject, $message, $headers );

            if ( $email_admin ){
                if ( $mail ) 
                    $sent[$to] = $email->owed; 
                else 
                    $notsent[$to] = $email->owed;
            }
        
        }

        if ( $email_admin ){

            $msg = $this->sent_summary_text( 'Payment reminder summary', $sent, $notsent );

            $mail = wp_mail( $options['email_admin'], 'Payment reminder summary', $msg, $headers );

        }

        
    }

    function filter_myaccount( $text ){

        if ( strpos($text, '%%myaccount%%') !== false ){

            global $nurc_emails;

            $colour = Nurc_Emails::$email_sender_options['base-colour'];
            $url = '<a style="color:' . $colour . '" href="' . home_url('shop/myaccount') . '" >My Account</a>';
            $text = str_replace('%%myaccount%%', $url, $text);

        }

        return $text;
    }

    function filter_total( $text, $amount ){

        $amount = nurc_money( $amount );
        $text = str_replace( '%%total%%', $amount, $text );

        return $text;

    }

    function sent_summary_text( $subject, $sent, $notsent ){ 

        global $nurc_emails;

        ob_start(); ?>

        <table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
            <tbody>
        <?php if ( count($sent) > 0 ) : ?>
                <tr>
                    <th style="padding:2px 5px; text-align:left;">Emails sent:</th>
                    <th></th>
                </tr>
                <?php foreach ($sent as $email => $value) : ?>
                    <tr>
                        <td style="padding:2px 5px; text-align:left;"><?php echo $email; ?></td>
                        <td style="padding:2px 5px; text-align:right;"><?php echo nurc_money( $value ); ?></td>
                    </tr>
                <?php endforeach; ?>

        <?php endif; ?>
        <?php if ( count($notsent) > 0 ) : ?>
                <tr>
                    <th style="padding:2px 5px;">Emails failed to send:</th>
                    <th></th>
                </tr>
                <?php foreach ($notsent as $email => $value) : ?>
                    <tr>
                        <td style="padding:2px 5px; text-align:left;"><?php echo $email; ?></td>
                        <td style="padding:2px 5px; text-align:right;"><?php echo nurc_money( $value ); ?></td>
                    </tr>
                <?php endforeach; ?>

        <?php endif; ?>
            </tbody>
        </table>
    
        <?php $text = ob_get_contents();

        ob_end_clean();

        return $nurc_emails->set_message( $subject, $text); 
    }

    private function get_next_email_date( $frequency ){

        $today = intval( date('j') );
        $daydate = sprintf('%02d', $frequency);
        $date = 'today';

        if ( $frequency > $today ){
            $date = $daydate . '-' . date('M-Y');
        } elseif ( $frequency < $today ){
            $month = strtotime('+1 month');
            $date = $daydate . '-' . date('M-Y', $month);
        }

        return $date;
    }
}

new Nurc_Cron();