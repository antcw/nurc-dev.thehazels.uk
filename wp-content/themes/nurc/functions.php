<?php
/* Child theme functions file
 parent theme style handle 'nurc'

 colormag function overrides:
 	colormag_footer_copyright()
 	colormag_entry_meta()
 	colormag_social_links()

 templates:
 	front-page.php
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Define date format
if ( ! defined( 'NURC_DATE' ) ) {
	define( 'NURC_DATE', get_option( 'date_format' ) );
}
// Define datetime format
if ( ! defined( 'NURC_DATETIME' ) ) {
	define( 'NURC_DATETIME', NURC_DATE . ' ' . get_option( 'time_format' ) );
}

include('includes/widgets/nurc-single-post.php'); #widget for front page single post
include('includes/nurc-my-account.php');          #customisation of the woocommerce myaccount pages
include('includes/nurc-shop.php');                #customisation of woocommerce shop and product display
include('includes/nurc-admin-setup.php');         #fields, taxonomy for member
include('includes/nurc-private-racks.php');       #fields, taxonomy for private rack
include('includes/nurc-race-entries.php');        #fields, taxonomy for race entry
include('includes/nurc-admin-user.php');          #customised settings for NURC Admin user role
include('includes/nurc-cron.php');                #cron task: payment reminder, plus Nurc_Emails
if ( is_admin() ) {
	include('includes/nurc-admin-members.php');   #convert order to members custom post
	include('includes/nurc-payments.php');        #recording of payments for race entries etc (members quick edit)
}

add_action( 'wp_enqueue_scripts', 'nurc_enqueue' );
function nurc_enqueue() { 
	$parent_style = 'colormag';  
	//Enqueue parent and child theme style.css 
	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' ); 
	wp_enqueue_style( 'nurc-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ), wp_get_theme()->get('Version'));
	if ( is_singular('product') ) {
    	wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_enqueue_style('nurc_datepicker', get_stylesheet_directory_uri() . '/assets/css/jquery-ui.css', false, '1.0.0' );
		wp_enqueue_script( 'nurc-membership', get_stylesheet_directory_uri() . '/assets/js/nurc-membership.js', array('jquery'), 1 );
	}
	if ( is_page('my-account') ) {
    	wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_enqueue_style('nurc_datepicker', get_stylesheet_directory_uri() . '/assets/css/jquery-ui.css', false, '1.0.0' );
		wp_enqueue_script( 'nurc-myaccount', get_stylesheet_directory_uri() . '/assets/js/nurc-myaccount.js', array('jquery'), 1.1 );
	}
}
add_action( 'admin_enqueue_scripts', 'nurc_admin_enqueue' );
function nurc_admin_enqueue($hook) {
	global $post_type, $pagenow;

	if ( $post_type == 'nurc' ) {
        wp_enqueue_script( 'nurc-admin', get_stylesheet_directory_uri() . '/assets/js/nurc-admin.js', array('jquery'), 1.3 );
		wp_localize_script( 'nurc-admin', 'wp_ajax', array( 
			'ajaxurl'   => admin_url( 'admin-ajax.php' ), 
			'ajaxnonce' => wp_create_nonce( 'ajax_post_validation' ),
			) 
		);
        wp_enqueue_style( 'nurc-print', get_stylesheet_directory_uri() . '/assets/css/nurc-style-print.css', false, '1.0.0', 'print' );
    }
	if ( $post_type == 'nurc_race' ) {
        wp_enqueue_script( 'nurc-race', get_stylesheet_directory_uri() . '/assets/js/nurc-race-entry.js', array('jquery-ui-autocomplete'), 1.4 );
		wp_localize_script( 'nurc-race', 'wp_ajax', array( 
			'ajaxurl'   => admin_url( 'admin-ajax.php' ), 
			'ajaxnonce' => wp_create_nonce( 'ajax_post_validation' ),
			) 
		);
    }
    if ( $post_type == 'nurc' || $post_type == 'nurc_race' ) {
        wp_enqueue_style( 'nurc-membership', get_stylesheet_directory_uri() . '/assets/css/nurc-style-admin.css', false, '1.0.0' );
        wp_enqueue_style( 'nurc_datepicker',  get_stylesheet_directory_uri() . '/assets/css/datepicker.css', false, '1.0.0' );
        wp_enqueue_script( 'jquery-ui-datepicker' ); 
    }
    if ( $post_type == 'nurc_email' ) {
        wp_enqueue_style( 'nurc-membership', get_stylesheet_directory_uri() . '/assets/css/nurc-style-admin-email.css', false, '1.0.0' );
    }
    if ( $post_type == 'nurc_rack' ){
        wp_enqueue_style( 'nurc-membership', get_stylesheet_directory_uri() . '/assets/css/nurc-style-admin.css', false, '1.0.0' );
        wp_enqueue_script( 'nurc-rack', get_stylesheet_directory_uri() . '/assets/js/nurc-rack.js', array('jquery-ui-autocomplete'), 1.2 );
		wp_localize_script( 'nurc-rack', 'wp_ajax', array( 
			'ajaxurl'   => admin_url( 'admin-ajax.php' ), 
			'ajaxnonce' => wp_create_nonce( 'ajax_post_validation' ),
			) 
		);
    }
    if ( $pagenow == 'plugins.php' ){
        wp_enqueue_style( 'nurc-membership', get_stylesheet_directory_uri() . '/assets/css/nurc-style-plugins-admin.css', false, '1.0' );
    }

}

#favicon
add_action('wp_head', 'nurc_favicon');
function nurc_favicon() {
    ?>
    <link rel="shortcut icon" href="<?php echo esc_url( home_url( '/favicon.ico' ) ); ?>" type="image/x-icon">
    <?php
}

/**
 * function to show the footer info, copyright information
 */
function colormag_footer_copyright() {
	$site_link = '<a href="' . esc_url( home_url( '/' ) ) . '" title="' . esc_attr( get_bloginfo( 'name', 'display' ) ) . '" ><span>' . get_bloginfo( 'name', 'display' ) . '</span></a>';

	$default_footer_value = sprintf( __( '&copy; %1$s %2$s', 'colormag' ), date( 'Y' ), $site_link );

	$colormag_footer_copyright = '<div class="copyright">' . $default_footer_value . '</div>';
	echo $colormag_footer_copyright;
}

/**
 * Shows meta information of post.
 */
function colormag_entry_meta() {
	if ( 'post' == get_post_type() ) :
		echo '<div class="below-entry-meta">';
		?>

		<?php
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string .= '<time class="updated" datetime="%3$s">%4$s</time>';
		}
		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);
		printf( __( '<span class="posted-on"><a href="%1$s" title="%2$s" rel="bookmark"><i class="fa fa-calendar-o"></i> %3$s</a></span>', 'colormag' ),
			esc_url( get_permalink() ),
			esc_attr( get_the_time() ),
			$time_string
		); ?>

		<?php
		if ( ! post_password_required() && comments_open() ) { ?>
			<span class="comments"><?php comments_popup_link( __( '<i class="fa fa-comment"></i> 0 Comments', 'colormag' ), __( '<i class="fa fa-comment"></i> 1 Comment', 'colormag' ), __( '<i class="fa fa-comments"></i> % Comments', 'colormag' ) ); ?></span>
		<?php }
		$tags_list = get_the_tag_list( '<span class="tag-links"><i class="fa fa-tags"></i>', __( ', ', 'colormag' ), '</span>' );
		if ( $tags_list ) {
			echo $tags_list;
		}

		edit_post_link( __( 'Edit', 'colormag' ), '<span class="edit-link"><i class="fa fa-edit"></i>', '</span>' );

		echo '</div>';
	endif;
}

/* admin menu */
#https://www.easywebdesigntutorials.com/reorder-left-admin-menu-and-add-a-custom-user-role/
add_action( 'admin_menu', 'nurc_remove_admin_menu_items', 999 );
function nurc_remove_admin_menu_items() {
	remove_menu_page('gallery-lightbox-settings');     #Gallery Lightbox
}


/* archives */
#get a list of event links to show  before body of events archive page
add_action( 'colormag_before_body_content', 'nurc_add_event_links' );
function nurc_add_event_links(){

	if ( is_category('events') ) {
		$links = nurc_get_links('events');

		if (  $links ) { 
			$cat = get_term_by('slug', 'events', 'link_category');
			?>
			<div class="clearfix">
				<header class="page-header">
					<h1 class="page-title">
						<span><?php echo $cat->name; ?></span>
					</h1>
				</header>
				<div class="entry-content"><ul>
				<?php
					foreach ( $links as $link ) {
						?>
						<li><a href="<?php echo $link->link_url; ?>" target="<?php echo $link->link_target; ?>"><?php echo $link->link_name; ?></a>
							<span class="nurc_description"><?php echo $link->link_description; ?></span>
						</li>
						<?php
					}
				?>
				</ul></div>
			</div>
			<?php
		}		
	}

}
function nurc_get_links($category) {

	global $wpdb;

	$sql = "SELECT * FROM $wpdb->links li 
			inner join $wpdb->term_relationships tr on tr.object_id = li.link_id
			inner join $wpdb->term_taxonomy tt on tr.term_taxonomy_id = tt.term_taxonomy_id
			inner join $wpdb->terms te on tt.term_id = te.term_id
			where tt.taxonomy = 'link_category' and (te.slug ='$category' or te.term_id = '$category')"; 
	$links = $wpdb->get_results($sql);

	if ( $wpdb->num_rows > 0 ) {
		return $links;
	} else {
		return false;
	}

}


/*
 * Breaking News/Latest Posts ticker section in the header
 */
if ( ! function_exists( 'colormag_breaking_news' ) ) :
	function colormag_breaking_news() {
		$post_status = 'publish';
		if ( get_option( 'fresh_site' ) == 1 ) {
			$post_status = array( 'auto-draft', 'publish' );
		}

		$get_featured_posts = new WP_Query( array(
			'posts_per_page'      => 5,
			'post_type'           => 'post',
			'ignore_sticky_posts' => true,
			'post_status'         => $post_status,
			'category_name'       => 'News',
		) );
		?>
		<div class="breaking-news">
			<strong class="breaking-news-latest"><?php _e( 'Latest:', 'colormag' ); ?></strong>
			<ul class="newsticker">
				<?php while ( $get_featured_posts->have_posts() ):$get_featured_posts->the_post(); ?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
					</li>
				<?php endwhile; ?>
			</ul>
		</div>
		<?php
		// Reset Post Data
		wp_reset_query();
	}
endif;


#https://docs.woocommerce.com/document/allow-html-in-term-category-tag-descriptions/
foreach ( array( 'pre_term_description' ) as $filter ) {
    remove_filter( $filter, 'wp_filter_kses' );
}
 
foreach ( array( 'term_description' ) as $filter ) {
    remove_filter( $filter, 'wp_kses_data' );
}


#https://codex.wordpress.org/Customizing_the_Login_Form
add_action( 'login_enqueue_scripts', 'nurc_logo' );
function nurc_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/nurc-logo.png);
			height:200px;
			width:200px;
			background-size: 200px;
			background-repeat: no-repeat;
        	padding-bottom: 5px;
        }
    </style>
<?php }
add_filter( 'login_headerurl', 'nurc_logo_url' );
function nurc_logo_url() {
    return home_url();
}
add_filter( 'login_headertitle', 'nurc_logo_url_title' );
function nurc_logo_url_title() {
    return get_bloginfo('name');
}


#formats money - requires setlocale(LC_MONETARY, 'en_GB')
function nurc_money( $amount ) {
	$total = (is_numeric($amount)) ? utf8_encode(money_format( '%.2n', $amount )) : '-';
    return $total;
}
#positive values in green, negative red
function nurc_class( $amount ) {
    if ( $amount >= 0 ) return 'green';
    
    return 'red';
}

function nurc_calc_user_balance( $user_id ){

    $balance = 0;
    $members = get_members_for_user( $user_id );

    foreach ($members as $member) {
    	$transactions = unserialize($member->transactions);
    	if ( is_array($transactions) && count($transactions) > 0 ) {
    		$balance += $transactions[0]['balance'];
    	}
    }

    update_user_meta( $user_id, 'nurc-balance', $balance );

    return $balance;
}


#return comma separated taxonomy list
function nurc_taxonomy_list ( $id, $taxonomy ) {

	$the_terms = wp_get_post_terms( $id, $taxonomy );
	$the_list = '';

	foreach ($the_terms as $term) {
		$the_list .= $term->name . ', ';
	}

	return rtrim($the_list, ', ');
}


#debugging
// https://www.codeforest.net/debugging-wp-mail-like-a-boss-debugger
#add_action('wp_mail_failed', 'action_wp_mail_failed', 10, 1);
function action_wp_mail_failed($wp_error) 
{
    return error_log(print_r($wp_error, true));
}         
// print out sql queries run
#add_filter('posts_request','debug_post_request'); // debugging sql query of a post
function debug_post_request($sql_text) {
   var_dump($sql_text); 
}
