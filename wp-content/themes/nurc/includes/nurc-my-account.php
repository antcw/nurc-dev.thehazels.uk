<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/* additions to woocommerce my-account pages */

#https://wordpress.stackexchange.com/questions/274701/how-to-modify-navigation-menu-of-the-my-account-page-in-woocommerce
// Note the low hook priority, this should give to your other plugins the time to add their own items...
add_filter( 'woocommerce_account_menu_items', 'nurc_menu_items', 99, 1 );
function nurc_menu_items( $items ) {

    $new_items = array(
    //  endpoint   => label
        'membership'   => __( 'Membership', 'nurc' ),
        'race-entries' => __( 'Race Fees', 'nurc' ),
    );

    $new_items = array_slice( $items, 0, 1, true ) +
        $new_items +
        array_slice( $items, 1, count( $items ), true );

    if ( isset($new_items['dashboard']) ) unset($new_items['dashboard']);
    if ( isset($new_items['downloads']) ) unset($new_items['downloads']);

    return $new_items;
}

#https://stackoverflow.com/questions/38039616/woocommerce-assigning-an-endpoint-to-a-custom-template-in-my-account-pages/38056768#38056768
add_action( 'init', 'nurc_woocommerce_endpoints' );
function nurc_woocommerce_endpoints() {
    add_rewrite_endpoint( 'membership', EP_ROOT | EP_PAGES );
    add_rewrite_endpoint( 'view-member', EP_ROOT | EP_PAGES );
    add_rewrite_endpoint( 'race-entries', EP_ROOT | EP_PAGES );
}

add_filter( 'query_vars', 'nurc_woocommerce_query_vars', 0 );
function nurc_woocommerce_query_vars( $vars ) {
    $vars[] = 'membership';
    $vars[] = 'view-member';
    $vars[] = 'race-entries';

    return $vars;
}

add_action( 'wp_loaded', 'nurc_woocommerce_flush_rewrite_rules' );
function nurc_woocommerce_flush_rewrite_rules() {
    flush_rewrite_rules();
}


#$classes = apply_filters( 'woocommerce_account_menu_item_classes', $classes, $endpoint );

add_action( 'woocommerce_account_membership_endpoint', 'nurc_account_membership' );
add_action( 'woocommerce_account_view-member_endpoint', 'nurc_account_view_member' );
add_action( 'woocommerce_account_race-entries_endpoint', 'nurc_account_race_entries' );

/**
 * My Account > Membership template.
 *
 * @param int $current_page Current page number.
 */
function nurc_account_membership( $current_page ) {
	global $nurc;
	$id = get_current_user_id();

	$columns = array(
		'name'               => 'Name',
		'nurc_member'        => 'Membership',
		'nurc_member_type'   => 'Type',
		'nurc_member_status' => 'Status',
		'rack'               => 'Private rack'
	);

	wc_get_template(
		'myaccount/membership.php',
		array(
			'members'      => get_members_for_user( $id ),
			'url'          => wc_get_page_permalink( 'myaccount' ),
			'columns'      => $columns,
			'balance'      => nurc_calc_user_balance( $id ),
		)
	);
}

/**
 * My Account > Membership > Member template.
 *
 * @param int $current_page Current page number.
 */
function nurc_account_view_member( $current_page ) {
	global $nurc;
	$current_page    = empty( $current_page ) ? 1 : absint( $current_page );
	$id = get_query_var( 'view-member' );
	$fields = $nurc->get_membership_name( $id, 'nurc_member' );
    $meta = $nurc->get_nurc_meta( $id, $fields );

	wc_get_template(
		'myaccount/view-member.php',
		array(
			'current_page' => absint( $current_page ),
			'meta'         => get_post_meta( $id ),
			'fields'       => $fields,
			'nurc'         => $nurc,
			'id'           => $id,
			'meta'         => $meta,
		)
	);
}

/**
 * My Account > Race Entries template.
 *
 * @param int $current_page Current page number.
 */
function nurc_account_race_entries( $current_page ) {
	global $nurc;

	$columns = array(
		'date'    => 'Date',
		'fee'     => 'Fee',
		'payment' => 'Payment',
		'balance' => 'Balance',
		'note'    => 'Note',
	);
	$id = get_current_user_id();

	wc_get_template(
		'myaccount/race-entries.php',
		array(
			'members'      => get_members_for_user( $id ),
			'nurc'         => $nurc,
			'columns'      => $columns,
			'balance'      => nurc_calc_user_balance( $id ),
		)
	);
}


/*
 * Change endpoint title.
 *
 * @param string $title
 * @return string
 */
/*
 * Change endpoint title.
 *
 * @param string $title
 * @return string
 https://github.com/woocommerce/woocommerce/wiki/Customising-account-page-tabs#manipulating-the-content-displayed-on-endpoints
 */
add_filter( 'the_title', 'nurc_endpoint_titles' );
function nurc_endpoint_titles( $title ) {
	global $wp_query;

	$new_items = array(
    //  endpoint   => label
        'membership'   => __( 'Membership overview', 'nurc' ),
        'race-entries' => __( 'Race entries', 'nurc' ),
        'view-member'  => __( 'Membership details', 'nurc' ),
        'edit-account'  => __( 'Main contact details', 'nurc' ),
    );


	foreach( $new_items as $endpoint => $newtitle ) {

		$is_endpoint = isset( $wp_query->query_vars[$endpoint] );

		if ( $is_endpoint && ! is_admin() && is_main_query() && in_the_loop() && is_account_page() ) {
			// New page title.
			$title = __( $newtitle, 'woocommerce' );

			remove_filter( 'the_title', 'nurc_endpoint_titles' );
		}

	}

	return $title;
}


add_action( 'woocommerce_before_edit_account_form', 'nurc_notice' );
function nurc_notice(){
	?>
	<p>
		Edit main contact details here and individual membership details in the <a href="<?php echo home_url('/shop/my-account/membership') ?>">Membership tab</a>.
	</p>
	<?php
}


/**
 * Save the view member details and redirect back to membership page.
 */
add_action( 'init', 'save_view_member' );
function save_view_member() {

	if ( ! isset( $_SERVER['REQUEST_METHOD'] ) ) {
			return;
	}

	if ( 'POST' !== strtoupper( $_SERVER['REQUEST_METHOD'] ) ) {
		return;
	}

	if ( empty( $_POST['action'] ) || 'save_view_member' !== $_POST['action'] || empty( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'save_view_member' ) || ! isset($_POST['nurc-id']) ) {
		return;
	}

	wc_nocache_headers();

	$user_id = get_current_user_id();
	if ( $user_id <= 0 ) {
		return;
	}

	// Handle required fields.
	global $nurc;
	$id = intval($_POST['nurc-id']);

	if ( $id > 0 ) {
		$fields = $nurc->get_membership_name( $id, 'nurc_member' );

	    foreach ( $fields as $labels ) {

	        foreach ( $labels['fields'] as $key => $label ) {
	        	if ( isset($_POST[$key]) ) {
		            if ( $label[1] == 'textarea' ) {
		                $nurc_meta[$key] = sanitize_textarea_field($_POST[$key]);
		            } else {
		                $nurc_meta[$key] = sanitize_text_field($_POST[$key]);
		            }
	        	} else {
	        		$nurc_meta[$key] = null;
	        	}
	        }
	    }

	    foreach ($nurc_meta as $key => $value) {
	    	if ($value) {
	    		update_post_meta( $id, $key, $value );
	    	} else {
	    		delete_post_meta( $id, $key );
	    	}
	    }
		
		wc_add_notice( __( 'Membership details changed successfully.', 'woocommerce' ) );

		wp_safe_redirect( wc_get_page_permalink( 'myaccount' ) );
		exit;		
	}

}
