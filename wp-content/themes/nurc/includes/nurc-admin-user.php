<?php 
/*
 * Defines the role NURC Admin and customises toolbar, dashboard and user profile
 */

if (!defined( 'ABSPATH' )) { exit; }


add_action( 'admin_enqueue_scripts', 'nurc_admin_user_enqueue' );
function nurc_admin_user_enqueue($hook) {
    if ( $hook == 'users.php' || $hook == 'user-edit.php' || $hook == 'profile.php' ) {
        wp_enqueue_style( 'nurc-membership', get_stylesheet_directory_uri() . '/assets/css/nurc-style-user-admin.css', false, '1.1' );
    }
}


#https://wordpress.stackexchange.com/questions/49643/remove-personal-options-section-from-profile
// removes admin color scheme options
remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );

add_filter('user_contactmethods','hide_instant_messaging',10,1);
function hide_instant_messaging( $contactmethods ) {
	return array();
}



#add new user role NURC Admin
if ( ! wp_roles()->is_role('nurc_admin') ) {
	add_action( 'init', 'add_nurc_admin' );
}

function add_nurc_admin(){

	$capabilities = array(
        'read' => true,
        'edit_posts' => true,
        'edit_nurc' => true,
        'read_nurc' => true,
        'delete_nurc' => true,
        'delete_nurcs' => true,
        'edit_nurcs' => true,
        'edit_others_nurcs' => true,
        'publish_nurcs' => true,
        'read_private_nurcs' => true,
    );

    add_role( 'nurc_admin', 'NURC Admin', $capabilities );

    $role = get_role('administrator');
    foreach ($capabilities as $cap => $val) {
    	$role->add_cap($cap, $val);
    }
}


#remove menu items for NURC Admin, except for the NURC Members options
add_action( 'admin_menu', 'nurc_remove_menu_items' );
function nurc_remove_menu_items(){

	if ( !current_user_can('edit_others_posts') ){
		remove_menu_page( 'index.php' );         #dashboard
		remove_menu_page( 'edit.php' );          #posts
		remove_menu_page( 'edit-comments.php' ); #comments
		remove_menu_page( 'wpcf7' );             #contact
		remove_menu_page( 'tools.php' );         #tools	
	}
}


#trim menu bar to only show NURC members and Visit Site
add_action( 'admin_bar_menu', 'remove_wp_nodes', 999 );
function remove_wp_nodes() {

	if ( !current_user_can('edit_others_posts') ){

	    global $wp_admin_bar;
	    
        $wp_admin_bar->remove_node( 'site-name'); #don't want the toggle to dashboard
	    $wp_admin_bar->remove_node( 'new-content' ); #+New
	    $wp_admin_bar->remove_node( 'comments' ); #comments

        $wp_admin_bar->add_menu( array(
            'parent' => false, // use 'false' for a root menu, or pass the ID of the parent menu
            'id' => 'nurc', // link ID, defaults to a sanitized title value
            'title' => __( 'NURC Members' ), // link title
            'href' => admin_url( 'edit.php?post_type=nurc' ), // name of file
            'meta' => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
        ));

        if ( is_admin() ){

            $wp_admin_bar->add_menu( array(
                'parent' => false, // use 'false' for a root menu, or pass the ID of the parent menu
                'id' => 'view-site', // link ID, defaults to a sanitized title value
                'title' => __( 'Visit Site' ), // link title
                'href' => home_url(), // name of file
                'meta' => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
            ));

        }
	}
}


#calculate, save and display transaction balance for all members associated with the user

add_filter( 'manage_users_columns', 'nurc_add_user_column', 10, 1 );
function nurc_add_user_column($cols){

    unset( $cols['posts']);
    return array_merge( $cols, array( 
        'nurc-balance' => __('Balance'),
        ) 
    );
}

add_filter( 'manage_users_custom_column', 'nurc_users_custom_column', 10, 3 );
function nurc_users_custom_column( $val, $column_name, $userid ){

    switch ($column_name) {
        case 'nurc-balance':
            #$balance = nurc_calc_user_balance($userid);
            $balance = get_user_meta( $userid, 'nurc-balance', true );
            return '<span class="' . nurc_class($balance) . '">' . nurc_money( $balance ) . '</span>';
            break;
        default:
            return $val;
            break;
    }
}

function display_racks( $userid ){
    
    $racks = get_racks_for_user( $userid );
    $content = '';

    if ( $racks->have_posts() ) {
        while ( $racks->have_posts() ) {
            $racks->the_post();
            $id = get_the_id();

            $content .= '<a href="' . admin_url('post.php?post=' . $id . '&action=edit') . '">Rack no ' . get_post_meta($id, 'rack-no', true) . '</a><br>';
        }
    }
    return $content;
}

add_filter( 'manage_users_sortable_columns', 'nurc_users_sortable_columns' );
function nurc_users_sortable_columns($cols){

    $cols['nurc-balance'] = 'nurc-balance';

    return $cols;
}

add_action( 'pre_user_query', 'nurc_users_orderby' );
function nurc_users_orderby( $query ){

    global $current_screen;

    if (  ! is_null($current_screen) && $current_screen->base == 'users' ){

        if ( $orderby = $query->get('orderby') ) {

            switch ($orderby) {
                case 'nurc-balance':
                    global $wpdb;
                    $order = $query->get( 'order' );
                    $query->query_from   .= " INNER JOIN $wpdb->usermeta AS um ON ($wpdb->users.ID = um.user_id)";
                    $query->query_where  = "WHERE um.meta_key = 'nurc-balance' ";
                    $query->query_orderby = "ORDER BY um.meta_value+0 $order";
                    break;
                
                default:
                    break;
            }           
        }
    }
}

add_action( 'manage_users_extra_tablenav', 'nurc_display_extra_at_top' );
function nurc_display_extra_at_top( $which ){
    $balance = nurc_club_balance();
    ?>

    <div class="alignleft nurc-club-balance">
            Club balance: <span class="<?php echo nurc_class($balance); ?>"><?php echo nurc_money( $balance ); ?></span>
    </div>

<?php }

/**
 * Show custom user profile fields
 * 
 * @param  object $profileuser A WP_User object
 * @return void
 */
add_action( 'show_user_profile', 'custom_user_profile_fields', 10, 1 );
add_action( 'edit_user_profile', 'custom_user_profile_fields', 10, 1 );
function custom_user_profile_fields( $profileuser ) {
    $members = get_members_for_user($profileuser->ID);

?>
    <h2>NURC Details</h2>
    <table class="form-table">
        <tr>
            <th>
                <label>Associated members</label>
            </th>
            <td>
                <? if ( count($members) > 0 ) : ?>
                <table>
                    <?php foreach( $members as $member ) : 
                        $id = $member->ID;
                        $transactions = unserialize($member->transactions);
                        $balance = $transactions[0]['balance'];
                    ?>
                        <tr>
                            <td>
                                <a href="<?php echo admin_url('post.php?post=' . $id . '&action=edit'); ?>"><?php echo $member->firstname ?> <?php echo $member->surname; ?></a>
                            </td>
                            <td style="text-align: right;">
                                <span class="<?php echo nurc_class($balance); ?>"><?php echo nurc_money( $balance ); ?></span>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
                <?php else : ?>
                    None
                <?php endif; ?>
            </td>
        </tr>
    </table>
<?php
}

function get_members_for_user( $user_id ) {
    global $nurc, $wpdb;

    $query = "select p.ID, 
        (select m1.meta_value from wp_postmeta m1 where m1.meta_key='firstname' and m1.post_id = p.ID) as firstname,
        (select m1.meta_value from wp_postmeta m1 where m1.meta_key='surname' and m1.post_id = p.ID) as surname,
        (select m1.meta_value from wp_postmeta m1 where m1.meta_key='transactions' and m1.post_id = p.ID) as transactions
        from wp_posts p
        inner join wp_postmeta m on m.post_id = p.ID
        where post_author = $user_id and post_status = 'publish' and post_type = 'nurc'
        group by p.ID
        order by surname, firstname";

    return $wpdb->get_results($query);
}


function nurc_user( $post_id ) {

    $user_id = get_post_field( 'post_author', $post_id );

    if ( $user_id > 0 ) {
        return get_user_by('id', $user_id);
    } else {
        return false;
    }

}